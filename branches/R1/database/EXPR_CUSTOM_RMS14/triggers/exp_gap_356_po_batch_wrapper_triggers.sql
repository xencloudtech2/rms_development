/****************************************************************************************/
/*           Name: exp_gap_356_po_batch_wrapper_triggers.sql
/*    Object Type: Trigger Creation wrapper Script
/*    Description: This script calls the Trigger Creation scripts 
/*                 exp_gap_exp_cstcmpupds.pls and exp_gap_exp_cstcmpupdb.pls
/* Modification History:
/* Date         Name                      Modification Description
/* -----------  ------------------------  ------------------------
/* 18-SEP-2014  Baranidharan              Initial Version
/**************************************************************************************/

spool exp_gap_356_po_batch_wrapper_triggers.log

@exp_gap_exp_trg_cost_comp_upd_aiudr.trg
@exp_gap_exp_trg_ord_comp_assess_aur.trg

spool off
