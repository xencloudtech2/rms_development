/****************************************************************************************/
/*           Name: exp_gap_356_po_batch_wrapper_package.sql
/*    Object Type: Package wrapper Script
/*    Description: This script calls the Package Creation scripts 
/*                 exp_gap_exp_cstcmpupds.pls and exp_gap_exp_cstcmpupdb.pls
/* Modification History:
/* Date         Name                      Modification Description
/* -----------  ------------------------  ------------------------
/* 18-SEP-2014  Baranidharan              Initial Version
/**************************************************************************************/

spool exp_gap_356_po_batch_wrapper_package.log

@exp_gap_exp_cstcmpupds.pls
@exp_gap_exp_cstcmpupdb.pls

spool off
