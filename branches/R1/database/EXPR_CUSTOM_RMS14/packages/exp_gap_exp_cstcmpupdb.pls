/*********************************************************************************************************/
/*        Name: exp_gap_exp_cstcmpupdb.pls
/* Object Type: Package Body
/* Description: Express Cost Componet Update SQL package body 
/*
/* Modification History:
/* Date         Name                      Modification Description
/* -----------  ------------------------  ------------------------
/* 20-MAY-2014  Baranidharan  G           Initial Version
/*********************************************************************************************************/

-------------------------------------------------------------------------------------------------------------------------
create or replace
PACKAGE BODY EXP_COST_COMP_UPD_SQL AS

-- Package plsql table definitions.
TYPE ordsku_assess_TBL        is table of   COST_COMP_EXC_LOG%ROWTYPE   INDEX BY BINARY_INTEGER;
TYPE orders_TBL               is table of   COST_COMP_EXC_LOG%ROWTYPE            INDEX BY BINARY_INTEGER;
TYPE rev_ord_TBL              is table of   REV_ORDERS%ROWTYPE                   INDEX BY BINARY_INTEGER;
TYPE mod_order_item_hts_TBL   is table of   MOD_ORDER_ITEM_HTS%ROWTYPE           INDEX BY BINARY_INTEGER;

L_system_options              SYSTEM_OPTIONS%ROWTYPE;
L_vdate                       PERIOD.VDATE%TYPE := GET_VDATE();
L_ship_to_date                ORDHEAD.PICKUP_DATE%TYPE;

---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_EXP_RECAL_ASSMT
-- Purpose: The function is for internal use and is called by the function UPDATE_ORDLOC_EXP. This function
--          updates the ordloc exp component values. It also calls ELC_CALC_SQL.CALC_COMP to
--          recalculate the order expenses depending on the upd_assess_ind it updates the associated
--          assessments as well,
--    Note: This is a private function.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_EXP_RECAL_ASSMT (O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_ordloc_exp_log_tbl       IN OUT orders_TBL,
                                 O_log_tbl_cnt              IN OUT NUMBER,
                                 O_mod_order_item_hts_tbl   IN OUT mod_order_item_hts_TBL,
                                 O_mod_order_tbl_cnt        IN OUT NUMBER,
                                 O_rev_orders_tbl           IN OUT rev_ord_TBL,
                                 O_rev_ord_tbl_cnt          IN OUT NUMBER,
                                 I_row_id                   IN ROWID,
                                 I_item                     IN ITEM_MASTER.ITEM%TYPE,
                                 I_supplier                 IN SUPS.SUPPLIER%TYPE,
                                 I_order_no                 IN ORDHEAD.ORDER_NO%TYPE,
                                 I_ord_stat                 IN ORDHEAD.STATUS%TYPE,
                                 I_pack_item                IN ORDLOC_EXP.PACK_ITEM%TYPE,
                                 I_location                 IN ORDLOC_EXP.LOCATION%TYPE,
                                 I_comp_id                  IN ELC_COMP.COMP_ID%TYPE,
                                 I_orig_country_id          IN ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                 I_curr_comp_rate           IN ORDLOC_EXP.COMP_RATE%TYPE,
                                 I_curr_comp_currency       IN ORDLOC_EXP.COMP_CURRENCY%TYPE,
                                 I_curr_per_count           IN ORDLOC_EXP.PER_COUNT%TYPE,
                                 I_curr_per_count_uom       IN ORDLOC_EXP.PER_COUNT_UOM%TYPE,
                                 I_old_comp_rate            IN ORDLOC_EXP.COMP_RATE%TYPE,
                                 I_old_comp_currency        IN ORDLOC_EXP.COMP_CURRENCY%TYPE,
                                 I_old_per_count            IN ORDLOC_EXP.PER_COUNT%TYPE,
                                 I_old_per_count_uom        IN ORDLOC_EXP.PER_COUNT_UOM%TYPE,
                                 I_exchange_rate            IN ORDLOC_EXP.EXCHANGE_RATE%TYPE,
                                 I_new_comp_rate            IN ORDLOC_EXP.COMP_RATE%TYPE,
                                 I_new_comp_currency        IN ORDLOC_EXP.COMP_CURRENCY%TYPE,
                                 I_new_per_count            IN ORDLOC_EXP.PER_COUNT%TYPE,
                                 I_new_per_count_uom        IN ORDLOC_EXP.PER_COUNT_UOM%TYPE)
RETURN BOOLEAN
IS
   cursor C_ord_assessments(I_order_no ORDHEAD.ORDER_NO%TYPE, I_item ITEM_MASTER.ITEM%TYPE, I_comp_id ELC_COMP.COMP_ID%TYPE)
   is
   select distinct
          oh.order_no,
          oh.item,
          oha.nom_flag_2,
          oha.cvb_code,
          oh.hts,
          oh.import_country_id,
          oh.effect_from,
          oh.effect_to
     from ordsku_hts        oh,
          ordsku_hts_assess oha,
          cvb_detail        cd
    where oh.order_no  = I_order_no
      and oh.item      = I_item
      and oh.effect_from <= L_vdate
      and oh.effect_to   >= L_vdate
      and oha.order_no = oh.order_no
      and oha.seq_no   = oh.seq_no
      and cd.cvb_code  = oha.cvb_code
      and cd.comp_id   = I_comp_id
    order by order_no, item;

    L_ord_in_rev_tbl  VARCHAR2(1);

BEGIN
   --
   if I_curr_comp_rate     != I_old_comp_rate or
      I_curr_comp_currency != I_old_comp_currency or
      NVL(I_curr_per_count, -999)     != NVL(I_old_per_count, -999) or
      NVL(I_curr_per_count_uom, -999) != NVL(I_old_per_count_uom, -999) then

      O_ordloc_exp_log_tbl(O_log_tbl_cnt).order_no          := I_order_no;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).item              := I_item;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).pack_item         := I_pack_item;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).comp_id           := I_comp_id;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).location          := I_location;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).old_comp_rate     := I_curr_comp_rate;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).old_comp_currency := I_curr_comp_currency;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).old_per_count     := I_curr_per_count;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).old_per_count_uom := I_curr_per_count_uom;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).new_comp_rate     := I_new_comp_rate;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).new_comp_currency := I_new_comp_currency;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).new_per_count     := I_new_per_count;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).new_per_count_uom := I_new_per_count_uom;
      O_ordloc_exp_log_tbl(O_log_tbl_cnt).reason_code       := 'ORDU';
      O_log_tbl_cnt := O_log_tbl_cnt +1;

   end if;
   --
   -- Update the perticular Ordloc expense.
   update ordloc_exp oe
      set oe.comp_rate         = I_new_comp_rate,
          oe.comp_currency     = I_new_comp_currency,
          oe.per_count         = I_new_per_count,
          oe.per_count_uom     = I_new_per_count_uom,
          oe.exchange_rate     = decode(oe.comp_currency, I_new_comp_currency, oe.exchange_rate, I_exchange_rate)
    where oe.rowid = I_row_id;
   --

   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'PE',
                             I_item, -- item
                             I_supplier,
                             NULL, -- item_exp_type
                             NULL, -- item_exp_seq
                             I_order_no,
                             NULL, -- ord_seq_no
                             I_pack_item, -- pack_item
                             NULL, -- zone_id
                             I_location,
                             NULL, -- hts
                             NULL, -- import_country_id
                             I_orig_country_id,
                             NULL, -- effect_from
                             NULL) = FALSE then -- effect_to
      return FALSE;
   end if;
   --
   -- updating all the related assessments
   for assmnt in C_ord_assessments(I_order_no, I_item, I_comp_id) loop
      if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                'PA',
                                I_item, -- item
                                NULL,
                                NULL, -- item_exp_type
                                NULL, -- item_exp_seq
                                I_order_no,
                                NULL, -- ord_seq_no
                                I_pack_item, -- pack_item
                                NULL, -- zone_id
                                NULL,
                                assmnt.hts, -- hts
                                assmnt.import_country_id, -- import_country_id
                                I_orig_country_id,
                                assmnt.effect_from, -- effect_from
                                assmnt.effect_to) = FALSE then -- effect_to
         return FALSE;
      end if;

      O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).order_no            := I_order_no;
      O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).item                := I_item;
      O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).import_country_id   := assmnt.import_country_id;
      O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).hts                 := assmnt.hts;
      O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).effect_from         := assmnt.effect_from;
      O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).effect_to           := assmnt.effect_to;
      O_mod_order_tbl_cnt := O_mod_order_tbl_cnt+1;
      --

   end loop;
   --
   -- Make an entry into the rev table, if it is not present in db table of the plsql table.
   L_ord_in_rev_tbl := 'N';
   for cnt in 1..O_rev_ord_tbl_cnt -1 loop
      if O_rev_orders_tbl(cnt).order_no = I_order_no then
         L_ord_in_rev_tbl := 'Y';
         EXIT;
      end if;
   end loop;
   --
   if L_ord_in_rev_tbl = 'N' then
      O_rev_orders_tbl(O_rev_ord_tbl_cnt).order_no := I_order_no;
      O_rev_ord_tbl_cnt := O_rev_ord_tbl_cnt + 1;
   end if;
   --
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'EXP_COST_COMP_UPD_SQL.UPDATE_EXP_RECAL_ASSMT',
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_EXP_RECAL_ASSMT;


---------------------------------------------------------------------------------------------
-- Function Name: FLUSH_CHANGES_EXPENSES
-- Purpose: The function is called by the function UPDATE_ORDLOC_EXP. This function
--          copies all the data from the plsql tables sent as parameters to the respective tables.
--          The orders which were updated manually are inserted into the table ordloc_exp_exc_log
--          When an approved order is updated or modified are stored into the table rev_orders.
---------------------------------------------------------------------------------------------
FUNCTION FLUSH_CHANGES_EXPENSES(O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                I_ordloc_exp_log_tbl     IN      orders_TBL,
                                I_rev_orders_tbl         IN      rev_ord_TBL,
                                I_mod_order_item_hts_tbl IN      mod_order_item_hts_TBL)
RETURN BOOLEAN
IS
BEGIN
   for rec in 1..I_ordloc_exp_log_tbl.COUNT loop
      insert into cost_comp_exc_log
        (exception_type,
         order_no,
         item,
         pack_item,
         location,
         comp_id,
         old_comp_rate,
         old_comp_currency,
         old_per_count,
         old_per_count_uom,
         new_comp_rate,
         new_comp_currency,
         new_per_count,
         new_per_count_uom,
         create_id,
         create_datetime,
         reason_code)
        values
        ('OE',
         I_ordloc_exp_log_tbl(rec).order_no,
         I_ordloc_exp_log_tbl(rec).item,
         I_ordloc_exp_log_tbl(rec).pack_item,
         I_ordloc_exp_log_tbl(rec).location,
         I_ordloc_exp_log_tbl(rec).comp_id,
         I_ordloc_exp_log_tbl(rec).old_comp_rate,
         I_ordloc_exp_log_tbl(rec).old_comp_currency,
         I_ordloc_exp_log_tbl(rec).old_per_count,
         I_ordloc_exp_log_tbl(rec).old_per_count_uom,
         I_ordloc_exp_log_tbl(rec).new_comp_rate,
         I_ordloc_exp_log_tbl(rec).new_comp_currency,
         I_ordloc_exp_log_tbl(rec).new_per_count,
         I_ordloc_exp_log_tbl(rec).new_per_count_uom,
         user,
         sysdate,
         I_ordloc_exp_log_tbl(rec).reason_code);
   end loop;
   --
   --
   for rec in 1..I_rev_orders_tbl.COUNT loop
      merge into rev_orders ro
      using (select 'x' from dual)
      on    (ro.order_no = I_rev_orders_tbl(rec).order_no)
      when not matched then
         insert (order_no)
         values (I_rev_orders_tbl(rec).order_no);
   end loop;
   --
   --
   for rec in 1..I_mod_order_item_hts_tbl.COUNT loop

      -- insert the record if the records do not exist
      merge into mod_order_item_hts moi
      using (select 'x' from dual)
         on (moi.order_no          = I_mod_order_item_hts_tbl(rec).order_no
         and moi.item              = I_mod_order_item_hts_tbl(rec).item
         and moi.import_country_id = I_mod_order_item_hts_tbl(rec).import_country_id
         and moi.hts               = I_mod_order_item_hts_tbl(rec).hts
         and moi.effect_from       = I_mod_order_item_hts_tbl(rec).effect_from
         and moi.effect_to         = I_mod_order_item_hts_tbl(rec).effect_to
         and moi.unapprove_ind     = 'N'
         and moi.pgm_name          = 'batch_ordcostcompupd'
         and to_char(moi.updated_datetime,'DD-MON-YY')  = to_char(L_vdate,'DD-MON-YY'))
      when not matched then
       insert (order_no,
               item,
               import_country_id,
               hts,
               effect_from,
               effect_to,
               unapprove_ind,
               pgm_name,
               updated_datetime)
              values
              (I_mod_order_item_hts_tbl(rec).order_no,
               I_mod_order_item_hts_tbl(rec).item,
               I_mod_order_item_hts_tbl(rec).import_country_id,
               I_mod_order_item_hts_tbl(rec).hts,
               I_mod_order_item_hts_tbl(rec).effect_from,
               I_mod_order_item_hts_tbl(rec).effect_to,
               'N',
               'batch_ordcostcompupd',
               L_vdate);

   end loop;

   return TRUE;
   --
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'EXP_COST_COMP_UPD_SQL.FLUSH_CHANGES_EXPENSES',
                                            to_char(SQLCODE));
      return FALSE;
END FLUSH_CHANGES_EXPENSES;

/* Need to plug in the update_ordloc_exp function here */ -- GAP356 Modification
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ORDLOC_EXP
-- Purpose: The function is called by the function PROCESS_COST_COMP_UPDATES. This function
--          selects rows from the cost_comp_upd_stg which have the defaulting levels as 'I'tem,
--          'E'lc, 'S'upplier, 'C'ountry or 'P'artner. It updates the Order expenses and associated
--          assessments.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDLOC_EXP(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           I_Thread_no     IN      RESTART_CONTROL.NUM_THREADS%TYPE,
                           I_Threads       IN      RESTART_CONTROL.NUM_THREADS%TYPE)
RETURN BOOLEAN
IS

   L_ordloc_exp_log_tbl                   orders_TBL;  -- to hold the orders which are shipped or updated

   L_rev_orders_tbl                       rev_ord_TBL; -- holds the orders that need to be inserted into the REV Table.
   L_mod_order_item_hts_tbl               mod_order_item_hts_TBL; -- holds the assessments which were recalculated.

   -- Cursor used to loop through the records of the GTT.
   cursor C_ord_items
   is
   select gtt.row_id,
          gtt.order_no,
          gtt.item,
          gtt.supplier,
          gtt.origin_country_id,
          gtt.pack_item,
          gtt.location,
          gtt.comp_id,
          gtt.curr_comp_rate,
          gtt.curr_comp_currency,
          gtt.curr_per_count,
          gtt.curr_per_count_uom,
          gtt.old_comp_rate,
          gtt.old_comp_currency,
          gtt.old_per_count,
          gtt.old_per_count_uom,
          gtt.new_comp_rate,
          gtt.new_comp_currency,
          gtt.new_per_count,
          gtt.new_per_count_uom,
          gtt.shipped_ind,
          gtt.order_status,
          gtt.nom_flag_2,
          gtt.cvb_code
     from gtt_cost_comp_upd gtt
     order by order_no, item;

   -- Cursor used to fetch the pick up date (Ship to date)

   cursor C_get_ship_to_date (I_order_no ORDHEAD.ORDER_NO%TYPE)
   is
   select pickup_date
     from ordhead
    where order_no = I_order_no;

   -- cursor used to fetch all the assessments which are needed to be updated due to the change in the expense.
   cursor C_ord_assessments(I_order_no ORDHEAD.ORDER_NO%TYPE, I_item ITEM_MASTER.ITEM%TYPE, I_comp_id ELC_COMP.COMP_ID%TYPE)
   is
   select oh.order_no,
          oh.item,
          oha.nom_flag_2,
          oha.cvb_code,
          oh.hts,
          oh.import_country_id,
          oh.effect_from,
          oh.effect_to
     from ordsku_hts        oh,
          ordsku_hts_assess oha,
          cvb_detail        cd
    where oh.order_no  = I_order_no
      and oh.item      = I_item
   -- and oh.effect_from <= L_vdate
   -- and oh.effect_to   >= L_vdate
      and oh.effect_from <= L_ship_to_date
      and oh.effect_to   >= L_ship_to_date
      and oha.order_no = oh.order_no
      and oha.seq_no   = oh.seq_no
      and cd.cvb_code  = oha.cvb_code
      and cd.comp_id   = I_comp_id
    order by order_no, item, oha.seq_no;
   L_ord_assessments  C_ord_assessments%ROWTYPE;

   -- cursor to fetch all the hts codes associated to the order.
   cursor C_ord_hts(I_order_no ORDHEAD.ORDER_NO%TYPE, I_item ITEM_MASTER.ITEM%TYPE)
   is
   select oh.hts,
          oh.import_country_id,
          oh.effect_from,
          oh.effect_to
     from ordsku_hts oh
    where oh.order_no     = I_order_no
      and oh.item         = I_item
    --and oh.effect_from <= L_vdate
    --and oh.effect_to   >= L_vdate;
      and oh.effect_from <= L_ship_to_date
      and oh.effect_to   >= L_ship_to_date;


   L_prev_ord           ORDHEAD.ORDER_NO%TYPE := NULL;
   L_prevord_status     VARCHAR2(1)           := NULL;
   L_log_tbl_cnt        NUMBER := 1;
   L_rev_ord_tbl_cnt    NUMBER := 1;
   L_mod_order_tbl_cnt  NUMBER := 1;

   -- Cursor to fetch the custom entries.
   cursor C_custom_entry(I_ord_no ORDHEAD.ORDER_NO%TYPE, I_item ITEM_MASTER.ITEM%TYPE)
   is
   select distinct ceh.ce_id,
          coi.vessel_id,
          coi.voyage_flt_id,
          coi.estimated_depart_date,
          NVL(coi.manifest_item_qty, 0) manifest_item_qty,
          NVL(coi.manifest_item_qty_uom, 0) manifest_item_qty_uom,
          ceh.status,
          oh.supplier
     from ce_ord_item coi,
          ce_head ceh,
          ordhead oh
    where oh.order_no  = I_ord_no
      and coi.order_no = oh.order_no
      and coi.item     = I_item
      and ceh.ce_id    = coi.ce_id
    order by ceh.status desc;

   L_custom_entry C_custom_entry%ROWTYPE;

   -- Cursor to get the total ordered quantity
   cursor C_ord_qty(I_order ORDLOC.ORDER_NO%TYPE, I_item ORDLOC.ITEM%TYPE)
   is
   select SUM(ol.qty_ordered)
     from ordloc ol
    where ol.order_no = I_order
      and ol.item     = I_item;

   L_error_message         RTK_ERRORS.RTK_KEY%TYPE;
   L_standard_uom          UOM_CLASS.UOM%TYPE;
   L_standard_class        UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor           ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_total_qty             NUMBER;
   L_std_manifest_item_qty NUMBER;
   L_ordered_qty           NUMBER;
   L_ord_in_rev_tbl        VARCHAR2(1);
   L_exchange_rate         ORDLOC_EXP.EXCHANGE_RATE%TYPE;
   L_written_date          DATE;
   --
BEGIN

   -- Populate the global temporary table with the selected order item records.
   insert into gtt_cost_comp_upd
   (      row_id,
          order_no,
          item,
          supplier,
          origin_country_id,
          pack_item,
          location,
          comp_id,
          curr_comp_rate,
          curr_comp_currency,
          curr_per_count,
          curr_per_count_uom,
          old_comp_rate,
          old_comp_currency,
          old_per_count,
          old_per_count_uom,
          new_comp_rate,
          new_comp_currency,
          new_per_count,
          new_per_count_uom,
          order_status,
          nom_flag_2,
          cvb_code,
          shipped_ind)
   with ordloc_exp1 as
   -- This select query gets the data defaulted from Item expense form and defaulted to the order expense.
     (select oe.rowid row_id,
             oe.order_no,
             oe.item,
             oh.supplier,
             os.origin_country_id,
             oe.pack_item,
             oe.location,
             oe.comp_id,
             oe.comp_rate            curr_comp_rate,
             oe.comp_currency        curr_comp_currency,
             oe.per_count            curr_per_count,
             oe.per_count_uom        curr_per_count_uom,
             ccu.old_comp_rate,
             ccu.old_comp_currency,
             ccu.old_per_count,
             ccu.old_per_count_uom,
             ccu.new_comp_rate,
             ccu.new_comp_currency,
             ccu.new_per_count,
             ccu.new_per_count_uom,
             oh.status,
             oe.nom_flag_2,
             oe.cvb_code
        from cost_comp_upd_stg ccu,
             ordhead             oh,
             ordsku              os,
             ordloc_exp          oe,
             ordloc              ol,
             exp_cost_comp_temp  ecct
       where ccu.defaulting_level  = 'I'
         and ccu.comp_type         = 'E'
         and ccu.order_default_ind = 'Y'
         and ccu.comp_id  = oe.comp_id
         and ccu.supplier = oh.supplier
         and ccu.item     = oe.item
         and oh.order_no  = oe.order_no
         and oh.status in ('W', 'S', 'A')
         and os.order_no  = oe.order_no
         and os.item      = oe.item
         and ((os.origin_country_id = ccu.origin_country_id
              and NVL(oh.lading_port,'-999') = NVL(ccu.lading_port,'-999')
              and NVL(oh.discharge_port,'-999') = NVL(ccu.discharge_port,'-999'))
              or exists (select 'x'
                           from cost_zone_group_loc cz
                          where cz.zone_group_id = ccu.zone_group_id
                            and cz.zone_id       = ccu.zone_id
                            and cz.location      = oe.location
                            and cz.location      = ol.location))
         and oh.pickup_date >= (select max(effective_date)
                                  from cost_comp_upd_stg ccus
                                 where ccus.comp_id  = ccu.comp_id)
         and oh.order_no   = ol.order_no
         and os.order_no   = ol.order_no
         and os.item       = ol.item
         and oe.order_no   = ol.order_no
         and oe.item       = ol.item
         and oe.location   = ol.location
         and oe.defaulted_from <> 'M'
         and ccu.comp_id   = ecct.comp_id
         and oe.comp_id    = ecct.comp_id
         and nvl(ol.qty_received,0) = 0
         and MOD(oe.order_no, I_Threads)+1 = I_Thread_no),

   ordloc_exp1a as -- queries only the shipped order from the data set ordloc_exp1
     (select oe.*,
               'Y'  -- shipped indicator.
          from ordloc_exp1 oe
         where exists (select 'x'
                 from shipment sh,
                      shipsku  ss
                where sh.shipment    = ss.shipment
                  and sh.order_no    = oe.order_no
                  and ss.item        = oe.item
                  and rownum =1)),

   ordloc_exp2 as
     -- the following query gets the data defaulted from Expense profile form when the updated is done at the Supplier level.
     (select oe.rowid row_id,
             oe.order_no,
             oe.item,
             oh.supplier,
             os.origin_country_id,
             oe.pack_item,
             oe.location,
             oe.comp_id,
             oe.comp_rate            curr_comp_rate,
             oe.comp_currency        curr_comp_currency,
             oe.per_count            curr_per_count,
             oe.per_count_uom        curr_per_count_uom,
             ccu.old_comp_rate,
             ccu.old_comp_currency,
             ccu.old_per_count,
             ccu.old_per_count_uom,
             ccu.new_comp_rate,
             ccu.new_comp_currency,
             ccu.new_per_count,
             ccu.new_per_count_uom,
             oh.status,
             oe.nom_flag_2,
             oe.cvb_code
        from cost_comp_upd_stg  ccu,
             exp_prof_head      eph,
             ordloc_exp         oe,
             ordhead            oh,
             ordsku             os,
             ordloc             ol,
             exp_cost_comp_temp ecct
       where ccu.defaulting_level  = 'S'
         and ccu.comp_type         = 'E'
         and ccu.order_default_ind = 'Y'
         and ccu.exp_prof_key      = eph.exp_prof_key
         and ccu.comp_id = oe.comp_id
         and oh.order_no = oe.order_no
         and oh.status in ('W', 'S', 'A')
         and oe.defaulted_from = 'S'
         and oe.key_value_1 = eph.key_value_1
         and ((os.origin_country_id = ccu.origin_country_id
              and NVL(oh.lading_port,'-999') = NVL(ccu.lading_port,'-999')
              and NVL(oh.discharge_port,'-999') = NVL(ccu.discharge_port,'-999'))
              or exists (select 'x'
                           from cost_zone_group_loc cz
                          where cz.zone_group_id = ccu.zone_group_id
                            and cz.zone_id       = ccu.zone_id
                            and cz.location      = oe.location
                            and cz.location      = ol.location))
         and os.order_no = oe.order_no
         and os.item     = oe.item
         and eph.module     = 'SUPP'
         and oh.pickup_date >= (select max(ccus.effective_date)
                                  from cost_comp_upd_stg ccus
                                 where ccus.comp_id  = ccu.comp_id)
         and oh.order_no   = ol.order_no
         and os.order_no   = ol.order_no
         and os.item       = ol.item
         and oe.order_no   = ol.order_no
         and oe.item       = ol.item
         and oe.location   = ol.location
         and ccu.comp_id   = ecct.comp_id
         and oe.comp_id    = ecct.comp_id
         and nvl(ol.qty_received,0) = 0
         and not exists (select 'x'    -- check that the same order exp may not be updated from an update done at Item exp and defaulted to Order.
                           from cost_comp_upd_stg ccu2
                          where ccu2.defaulting_level  = 'I'
                            and ccu2.comp_type         = 'E'
                            and ccu2.order_default_ind = 'Y'
                            and ccu2.comp_id  = ccu.comp_id
                            and ccu2.supplier = oh.supplier
                            and ccu2.item     = os.item
                            and ccu2.item     = ol.item
                            and ccu2.comp_id  = ecct.comp_id
                            and oh.pickup_date >= (select max(ccus.effective_date)
                                                     from cost_comp_upd_stg ccus
                                                    where ccus.comp_id  = ccu2.comp_id)
                            and ((os.origin_country_id = ccu2.origin_country_id
                                 and NVL(oh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                                 and NVL(oh.discharge_port,'-999') = NVL(ccu2.discharge_port,'-999'))
                               or exists (select 'x'
                                            from cost_zone_group_loc cz
                                           where cz.zone_group_id = ccu2.zone_group_id
                                             and cz.zone_id       = ccu2.zone_id
                                             and cz.location      = oe.location
                                             and cz.location      = ol.location)))
         and MOD(oe.order_no, I_Threads)+1 = I_Thread_no),

   ordloc_exp2a as -- queries order expense records with shipped orders.
     (select oe.*,
               'Y' -- shipped indicator
          from ordloc_exp2 oe
         where exists (select 'x'
                 from shipment sh,
                      shipsku  ss
                where sh.shipment    = ss.shipment
                  and sh.order_no    = oe.order_no
                  and ss.item        = oe.item
                  and rownum =1)),

   ordloc_exp3 as
     -- the following query gets the data defaulted from Expense profile form when the updated is done at the Partner level.
     (select oe.rowid row_id,
             oe.order_no,
             oe.item,
             oh.supplier,
             os.origin_country_id,
             oe.pack_item,
             oe.location,
             oe.comp_id,
             oe.comp_rate            curr_comp_rate,
             oe.comp_currency        curr_comp_currency,
             oe.per_count            curr_per_count,
             oe.per_count_uom        curr_per_count_uom,
             ccu.old_comp_rate,
             ccu.old_comp_currency,
             ccu.old_per_count,
             ccu.old_per_count_uom,
             ccu.new_comp_rate,
             ccu.new_comp_currency,
             ccu.new_per_count,
             ccu.new_per_count_uom,
             oh.status,
             oe.nom_flag_2,
             oe.cvb_code
        from exp_prof_head      eph,
             cost_comp_upd_stg  ccu,
             ordloc_exp         oe,
             ordhead            oh,
             ordsku             os,
             ordloc             ol,
             exp_cost_comp_temp ecct
       where ccu.defaulting_level  = 'P'
         and ccu.comp_type         = 'E'
         and ccu.order_default_ind = 'Y'
         and ccu.exp_prof_key      = eph.exp_prof_key
         and ccu.comp_id           = oe.comp_id
         and oh.order_no    = oe.order_no
         and oh.status in ('W','S','A')
         and oe.defaulted_from = 'P'
         and oe.key_value_1 = eph.key_value_1
         and oe.key_value_2 = eph.key_value_2
         and ((os.origin_country_id = ccu.origin_country_id
              and NVL(oh.lading_port,'-999') = NVL(ccu.lading_port,'-999')
              and NVL(oh.discharge_port,'-999') = NVL(ccu.discharge_port,'-999'))
              or exists (select 'x'
                           from cost_zone_group_loc cz
                          where cz.zone_group_id = ccu.zone_group_id
                            and cz.zone_id       = ccu.zone_id
                            and cz.location      = oe.location
                            and cz.location      = ol.location))
         and os.order_no = oe.order_no
         and os.item     = oe.item
         and eph.module  = 'PTNR'
         and not exists (select 'x'     -- check that the same order exp could not be updated from an update done at Item exp and defaulted to Order.
                           from cost_comp_upd_stg ccu2
                          where ccu2.defaulting_level  = 'I'
                            and ccu2.comp_type         = 'E'
                            and ccu2.order_default_ind = 'Y'
                            and ccu2.comp_id  = ccu.comp_id
                            and ccu2.supplier = oh.supplier
                            and ccu2.item     = os.item
                            and ccu2.comp_id  = ecct.comp_id
                            and ccu2.item     = ol.item
                            and oh.pickup_date  >= ( select max(ccus.effective_date)
                                                      from cost_comp_upd_stg ccus
                                                     where ccus.comp_id = ccu2.comp_id )
                            and ((os.origin_country_id = ccu2.origin_country_id
                                 and NVL(oh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                                 and NVL(oh.discharge_port,'-999') = NVL(ccu2.discharge_port,'-999'))
                                 or exists (select 'x'
                                              from cost_zone_group_loc cz
                                             where cz.zone_group_id = ccu2.zone_group_id
                                               and cz.zone_id       = ccu2.zone_id
                                               and cz.location      = oe.location
                                               and cz.location      = ol.location)))
         and oh.pickup_date  >= (select max(ccus.effective_date)
                                  from cost_comp_upd_stg ccus
                                 where ccus.comp_id  = ccu.comp_id)
         and oh.order_no   = ol.order_no
         and os.order_no   = ol.order_no
         and os.item       = ol.item
         and oe.order_no   = ol.order_no
         and oe.item       = ol.item
         and oe.location   = ol.location
         and ccu.comp_id   = ecct.comp_id
         and oe.comp_id    = ecct.comp_id
         and nvl(ol.qty_received,0) = 0
         and mod(oe.order_no, I_Threads)+1 = I_Thread_no),

   ordloc_exp3a as
     (select oe.*,
               'Y'
          from ordloc_exp3 oe
         where exists (select 'x'
                 from shipment sh,
                      shipsku  ss
                where sh.shipment    = ss.shipment
                  and sh.order_no    = oe.order_no
                  and ss.item        = oe.item
                  and rownum =1)),

   ordloc_exp4 as
   -- the following query gets the data which was defaulted at the expense profile country level.
     (select oe.rowid row_id,
             oe.order_no,
             oe.item,
             oh.supplier,
             os.origin_country_id,
             oe.pack_item,
             oe.location,
             oe.comp_id,
             oe.comp_rate           curr_comp_rate,
             oe.comp_currency       curr_comp_currency,
             oe.per_count           curr_per_count,
             oe.per_count_uom       curr_per_count_uom,
             ccu.old_comp_rate,
             ccu.old_comp_currency,
             ccu.old_per_count,
             ccu.old_per_count_uom,
             ccu.new_comp_rate,
             ccu.new_comp_currency,
             ccu.new_per_count,
             ccu.new_per_count_uom,
             oh.status,
             oe.nom_flag_2,
             oe.cvb_code
        from cost_comp_upd_stg     ccu,
             exp_prof_head         eph,
             ordloc_exp            oe,
             ordhead               oh,
             ordsku                os,
             ordloc                ol,
             exp_cost_comp_temp    ecct
       where ccu.defaulting_level  = 'C'
         and ccu.comp_type         = 'E'
         and ccu.order_default_ind = 'Y'
         and ccu.comp_id           = oe.comp_id
         and ccu.exp_prof_key      = eph.exp_prof_key
         and oe.key_value_1        = ccu.origin_country_id
         and NVL(oh.lading_port,'-999') = NVL(ccu.lading_port,'-999')
         and NVL(oh.discharge_port,'-999') = NVL(ccu.discharge_port,'-999')
         and oh.order_no = oe.order_no
         and oh.status in ('W','S','A')
         and oe.defaulted_from = 'C'
         and os.order_no = oe.order_no
         and os.item     = oe.item
         and eph.module  = 'CTRY'
         and not exists (select 'x'     -- check that the same order exp could not be updated from an update done at Item exp and defaulted to Order.
                           from cost_comp_upd_stg ccu2
                          where ccu2.defaulting_level  = 'I'
                            and ccu2.comp_type         = 'E'
                            and ccu2.order_default_ind = 'Y'
                            and ccu2.comp_id  = ccu.comp_id
                            and ccu2.supplier = oh.supplier
                            and ccu2.item     = os.item
                            and ccu2.comp_id  = ecct.comp_id
                            and ccu2.item     = ol.item
                            and oh.pickup_date  >= ( select max(ccus.effective_date)
                                                      from cost_comp_upd_stg ccus
                                                     where ccus.comp_id = ccu2.comp_id )
                            and ((os.origin_country_id = ccu2.origin_country_id
                                 and NVL(oh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                                 and NVL(oh.discharge_port,'-999') = NVL(ccu2.discharge_port,'-999'))
                                 or exists (select 'x'
                                              from cost_zone_group_loc cz
                                             where cz.zone_group_id = ccu2.zone_group_id
                                               and cz.zone_id       = ccu2.zone_id
                                               and cz.location      = oe.location
                                               and cz.location      = ol.location )))
         and oh.pickup_date >= (select max(ccus.effective_date)
                                  from cost_comp_upd_stg ccus
                                 where ccus.comp_id  = ccu.comp_id)
         and oh.order_no   = ol.order_no
         and os.order_no   = ol.order_no
         and os.item       = ol.item
         and oe.order_no   = ol.order_no
         and oe.item       = ol.item
         and oe.location   = ol.location
         and ccu.comp_id   = ecct.comp_id
         and oe.comp_id    = ecct.comp_id
         and nvl(ol.qty_received,0) = 0
         and mod(oe.order_no, I_Threads)+1 = I_Thread_no),

   ordloc_exp4a as
     (select oe.*,
               'Y'
          from ordloc_exp4 oe
         where exists (select 'x'
                 from shipment sh,
                      shipsku  ss
                where sh.shipment    = ss.shipment
                  and sh.order_no    = oe.order_no
                  and ss.item        = oe.item
                  and rownum =1)),

   ordloc_exp5 as
     -- the following query gets the data from Expense profile form when the updated is done at the ELC level.
     (select oe.rowid row_id,
             oe.order_no,
             oe.item,
             oh.supplier,
             os.origin_country_id,
             oe.pack_item,
             oe.location,
             oe.comp_id,
             oe.comp_rate           curr_comp_rate,
             oe.comp_currency       curr_comp_currency,
             oe.per_count           curr_per_count,
             oe.per_count_uom       curr_per_count_uom,
             ccu.old_comp_rate,
             ccu.old_comp_currency,
             ccu.old_per_count,
             ccu.old_per_count_uom,
             ccu.new_comp_rate,
             ccu.new_comp_currency,
             ccu.new_per_count,
             ccu.new_per_count_uom,
             oh.status,
             oe.nom_flag_2,
             oe.cvb_code
        from cost_comp_upd_stg  ccu,
             ordhead            oh,
             ordsku             os,
             ordloc_exp         oe,
             ordloc             ol,
             exp_cost_comp_temp ecct
       where ccu.defaulting_level  = 'E'
         and ccu.comp_type         = 'E'
         and ccu.order_default_ind = 'Y'
         and ccu.comp_id = oe.comp_id
         and oh.order_no = oe.order_no
         and oh.status in ('W', 'S', 'A')
         and os.order_no = oe.order_no
         and os.item     = oe.item
         and not exists ( select 'x'     -- check that the same order exp could not be updated from an update done at Item exp and defaulted to Order.
                            from cost_comp_upd_stg ccu2
                           where ccu2.defaulting_level  = 'I'
                             and ccu2.comp_type         = 'E'
                             and ccu2.order_default_ind = 'Y'
                             and ccu2.comp_id  = ccu.comp_id
                             and ccu2.supplier = oh.supplier
                             and ccu2.item     = os.item
                             and ccu2.comp_id  = ecct.comp_id
                             and ccu2.item     = ol.item
                             and oh.pickup_date >= ( select max(ccus.effective_date)
                                                      from cost_comp_upd_stg ccus
                                                     where ccus.comp_id = ccu2.comp_id )
                             and ((os.origin_country_id = ccu2.origin_country_id
                                  and NVL(oh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                                  and NVL(oh.discharge_port,'-999') = NVL(ccu2.discharge_port,'-999'))
                                 or exists (select 'x'
                                              from cost_zone_group_loc cz
                                             where cz.zone_group_id = ccu2.zone_group_id
                                               and cz.zone_id       = ccu2.zone_id
                                               and cz.location      = oe.location
                                               and cz.location      = ol.location ))
                             and rownum =1
                           UNION ALL
                          select 'x'
                            from cost_comp_upd_stg  ccu2,
                                 exp_prof_head      eph
                           where ccu2.defaulting_level  = 'S'
                             and ccu2.comp_type         = 'E'
                             and ccu2.order_default_ind = 'Y'
                             and ccu2.exp_prof_key      = eph.exp_prof_key
                             and ccu2.comp_id           = oe.comp_id
                             and ccu2.comp_id  = ecct.comp_id
                             and oh.pickup_date  >= ( select max(ccus.effective_date)
                                                      from cost_comp_upd_stg ccus
                                                     where ccus.comp_id = ccu2.comp_id )
                             and ((os.origin_country_id  = ccu2.origin_country_id
                                   and NVL(oh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                                   and NVL(oh.discharge_port,'-999') = NVL(ccu2.discharge_port,'-999'))
                                 or exists (select 'x'
                                              from cost_zone_group_loc cz
                                             where cz.zone_group_id = ccu2.zone_group_id
                                               and cz.zone_id       = ccu2.zone_id
                                               and cz.location      = oe.location
                                               and cz.location      = ol.location ))
                             and oe.key_value_1         = eph.key_value_1
                             and eph.module             = 'SUPP'
                             and rownum =1
                           UNION ALL
                          select 'x'
                            from cost_comp_upd_stg  ccu2,
                                 exp_prof_head      eph
                           where ccu2.defaulting_level  = 'P'
                             and ccu2.comp_type         = 'E'
                             and ccu2.order_default_ind = 'Y'
                             and ccu2.exp_prof_key      = eph.exp_prof_key
                             and ccu2.comp_id           = oe.comp_id
                             and ccu2.comp_id  = ecct.comp_id
                             and oh.pickup_date  >= ( select max(ccus.effective_date)
                                                      from cost_comp_upd_stg ccus
                                                     where ccus.comp_id = ccu2.comp_id )
                             and ((os.origin_country_id = ccu2.origin_country_id
                                   and NVL(oh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                                   and NVL(oh.discharge_port,'-999') = NVL(ccu2.discharge_port,'-999'))
                                 or exists (select 'x'
                                              from cost_zone_group_loc cz
                                             where cz.zone_group_id = ccu2.zone_group_id
                                               and cz.zone_id       = ccu2.zone_id
                                               and cz.location      = oe.location
                                               and cz.location      = ol.location ))
                             and oe.key_value_1         = eph.key_value_1
                             and oe.key_value_2         = eph.key_value_2
                             and eph.module             = 'PTNR'
                             and rownum =1
                           UNION ALL
                          select 'x'
                            from cost_comp_upd_stg  ccu2,
                                 exp_prof_head      eph
                           where ccu2.defaulting_level  = 'C'
                             and ccu2.comp_type         = 'E'
                             and ccu2.order_default_ind = 'Y'
                             and ccu2.exp_prof_key      = eph.exp_prof_key
                             and ccu2.comp_id           = oe.comp_id
                             and oe.key_value_1         = eph.origin_country_id
                             and NVL(oh.lading_port,'-999') = NVL(ccu2.lading_port,'-999')
                             and NVL(oh.discharge_port,'-999') = NVL(ccu2.discharge_port,'-999')
                             and eph.module             = 'CTRY'
                             and ccu2.comp_id  = ecct.comp_id
                             and oh.pickup_date  >= ( select max(ccus.effective_date)
                                                      from cost_comp_upd_stg ccus
                                                     where ccus.comp_id = ccu2.comp_id )
                             and rownum =1 )
         and oh.pickup_date  >= (select max(effective_date)
                                  from cost_comp_upd_stg ccus
                                 where ccus.comp_id  = ccu.comp_id)
         and oh.order_no   = ol.order_no
         and os.order_no   = ol.order_no
         and os.item       = ol.item
         and oe.order_no   = ol.order_no
         and oe.item       = ol.item
         and oe.location   = ol.location
         and oe.defaulted_from <> 'M'
         and ccu.comp_id   = ecct.comp_id
         and oe.comp_id    = ecct.comp_id
         and nvl(ol.qty_received,0) = 0
         and mod(oe.order_no, I_Threads)+1 = I_Thread_no),

   ordloc_exp5a as
     (select oe.*,
               'Y'
          from ordloc_exp5 oe
         where exists (select 'x'
                 from shipment sh,
                      shipsku  ss
                where sh.shipment    = ss.shipment
                  and sh.order_no    = oe.order_no
                  and ss.item        = oe.item
                  and rownum =1))

   select ordloc_exp1a.*
     from ordloc_exp1a
   union all
   select oe.*,
          'N'
     from ordloc_exp1 oe
    where oe.row_id not in (select oea.row_id
                              from ordloc_exp1a oea)
   union all
   select ordloc_exp2a.*
     from ordloc_exp2a
   union all
   select oe.*,
          'N'
     from ordloc_exp2 oe
    where oe.row_id not in (select oea.row_id
                              from ordloc_exp2a oea)
   union all
   select ordloc_exp3a.*
     from ordloc_exp3a
   union all
   select oe.*,
          'N'
     from ordloc_exp3 oe
    where oe.row_id not in (select oea.row_id
                              from ordloc_exp3a oea)
   union all
   select ordloc_exp4a.*
     from ordloc_exp4a
   union all
   select oe.*,
          'N'
     from ordloc_exp4 oe
    where oe.row_id not in (select oea.row_id
                              from ordloc_exp4a oea)
   union all
   select ordloc_exp5a.*
     from ordloc_exp5a
   union all
   select oe.*,
          'N'
     from ordloc_exp5 oe
    where oe.row_id not in (select oea.row_id
                              from ordloc_exp5a oea);

   -- In all the above queries we have selected the orders rowids along with the other required values into the Global temporary table.
   --
   if L_system_options.import_ind = 'N' then
      --
      -- Updating the orders.
      -- 1. Unapporve an order.
      -- 2. Update the ordloc expense.
      -- 3. Call ELC_CALC_SQL.CALC_COMP to recalculate the est_exp_value.
      -- 4. Approve the orders.
      --
      L_prev_ord       := NULL;
      L_prevord_status := NULL;
      --
      --
      for ord_item in C_ord_items loop
         if L_prev_ord is not NULL then
            if L_prev_ord != ord_item.order_no then
               -- New order being considered, hence approve the previous order
               if L_prevord_status = 'A' then
                  if OTB_SQL.ORD_APPROVE_CASCADE(L_prev_ord,
                                                 O_error_message) = FALSE then
                     return FALSE;
                  end if;
               end if;
               --
               -- Unapprove the new order.
               if ord_item.order_status = 'A' then
                  if OTB_SQL.ORD_UNAPPROVE_CASCADE(ord_item.order_no,
                                                   O_error_message) = FALSE then
                     return FALSE;
                  end if;
               end if;
               L_prev_ord       := ord_item.order_no;
               L_prevord_status := ord_item.order_status;
            end if;
         else
            if ord_item.order_status = 'A' then
               if OTB_SQL.ORD_UNAPPROVE_CASCADE(ord_item.order_no,
                                        O_error_message) = FALSE then
                  return FALSE;
               end if;
            end if;
            L_prev_ord       := ord_item.order_no;
            L_prevord_status := ord_item.order_status;
         end if;
         --
         --
         if ORDER_ATTRIB_SQL.GET_WRITTEN_DATE(O_error_message,
                                              L_written_date,
                                              ord_item.order_no) = FALSE then
            return FALSE;
         end if;
         --
         if CURRENCY_SQL.GET_RATE(O_error_message,
                                  L_exchange_rate,
                                  ord_item.new_comp_currency,
                                  'E',
                                  L_written_date) = false then
            return FALSE;
         end if;
         ---
         if ord_item.shipped_ind = 'Y' then
            -- Since the orders' item has been shipped, we put the record into the
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).item              := ord_item.item;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).pack_item         := ord_item.pack_item;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).location          := ord_item.location;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_currency := ord_item.curr_comp_currency;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_currency := ord_item.new_comp_currency;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).reason_code       := 'ORDS';
            L_log_tbl_cnt := L_log_tbl_cnt +1;
         else
            -- order/item is not shipped.
            if ord_item.curr_comp_rate       != ord_item.old_comp_rate or
               ord_item.curr_comp_currency   != ord_item.old_comp_currency or
               NVL(ord_item.curr_per_count, -999)       != NVL(ord_item.old_per_count, -999) or
               NVL(ord_item.curr_per_count_uom, -999)   != NVL(ord_item.old_per_count_uom, -999) then

               L_ordloc_exp_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).item              := ord_item.item;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).pack_item         := ord_item.pack_item;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).location          := ord_item.location;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_currency := ord_item.curr_comp_currency;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_currency := ord_item.new_comp_currency;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom;
               L_ordloc_exp_log_tbl(L_log_tbl_cnt).reason_code       := 'ORDU';
               L_log_tbl_cnt := L_log_tbl_cnt +1;

            end if;
            --
            -- Update the perticular Ordloc expense.
            update ordloc_exp oe
               set oe.comp_rate         = ord_item.new_comp_rate,
                   oe.comp_currency     = ord_item.new_comp_currency,
                   oe.per_count         = ord_item.new_per_count,
                   oe.per_count_uom     = ord_item.new_per_count_uom,
                   oe.exchange_rate     = decode(oe.comp_currency, ord_item.new_comp_currency, oe.exchange_rate, L_exchange_rate)
             where oe.rowid = ord_item.row_id;
            -- recalculate
            if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                      'PE',
                                      ord_item.item, -- item
                                      ord_item.supplier,
                                      NULL, -- item_exp_type
                                      NULL, -- item_exp_seq
                                      ord_item.order_no,
                                      NULL, -- ord_seq_no
                                      ord_item.pack_item, -- pack_item
                                      NULL, -- zone_id
                                      ord_item.location,
                                      NULL, -- hts
                                      NULL, -- import_country_id
                                      ord_item.origin_country_id,
                                      NULL, -- effect_from
                                      NULL) = FALSE then -- effect_to
               return FALSE;
            end if;
            --
            -- Make an entry into the rev table, if it is not present in db table of the plsql table.
            L_ord_in_rev_tbl := 'N';
            for cnt in 1..L_rev_ord_tbl_cnt-1 loop
               if L_rev_orders_tbl(cnt).order_no = ord_item.order_no then
                  L_ord_in_rev_tbl := 'Y';
                  EXIT;
               end if;
            end loop;
            --
            if L_ord_in_rev_tbl = 'N' then
               L_rev_orders_tbl(L_rev_ord_tbl_cnt).order_no := ord_item.order_no;
               L_rev_ord_tbl_cnt := L_rev_ord_tbl_cnt + 1;
            end if;            --
         end if; -- if order shipped.
      end loop;
      --
      if L_prev_ord is not NULL and L_prevord_status = 'A' then
         if OTB_SQL.ORD_APPROVE_CASCADE(L_prev_ord,
                                O_error_message) = FALSE then
            return FALSE;
         end if;
      end if;
      --
   else -- import ind = 'Y'
      --
      L_log_tbl_cnt     := 1;
      L_prev_ord        := NULL;
      L_prevord_status  := NULL;
      --
      -- Start looping through each of the order items captured in the global temporary table.
      for ord_item in C_ord_items loop
         --
         -- Approve the previous order if this is the second different order.
         if L_prev_ord is not NULL then
            if L_prev_ord != ord_item.order_no then
               -- New order being considered, hence approve the previous order
               if L_prevord_status = 'A' then

                  if OTB_SQL.ORD_APPROVE_CASCADE(L_prev_ord,
                                         O_error_message) = FALSE then
                     return FALSE;
                  end if;
               end if;
               --
               -- Unapprove the new order.
               if ord_item.order_status = 'A' then

                  if OTB_SQL.ORD_UNAPPROVE_CASCADE(ord_item.order_no,
                                           O_error_message) = FALSE then
                     return FALSE;
                  end if;
               end if;
               L_prev_ord       := ord_item.order_no;
               L_prevord_status := ord_item.order_status;
            end if;
         else
            if ord_item.order_status = 'A' then
               if OTB_SQL.ORD_UNAPPROVE_CASCADE(ord_item.order_no,
                                        O_error_message) = FALSE then
                  return FALSE;
               end if;
            end if;
            L_prev_ord        := ord_item.order_no;
            L_prevord_status  := ord_item.order_status;
         end if;
         --
         --
         if ORDER_ATTRIB_SQL.GET_WRITTEN_DATE(O_error_message,
                                              L_written_date,
                                              ord_item.order_no) = FALSE then
            return FALSE;
         end if;
         --
         if CURRENCY_SQL.GET_RATE(O_error_message,
                                  L_exchange_rate,
                                  ord_item.new_comp_currency,
                                  'E',
                                  L_written_date) = false then
            return FALSE;
         end if;
         --
         -- Getting the L_ship_to_date
            open C_get_ship_to_date(ord_item.order_no);
            fetch C_get_ship_to_date into L_ship_to_date;
            close C_get_ship_to_date;


         if ord_item.shipped_ind = 'Y' then
            -- Since the orders' item has been shipped, we put the record into the
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).item              := ord_item.item;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).pack_item         := ord_item.pack_item;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).location          := ord_item.location;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_currency := ord_item.curr_comp_currency;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_currency := ord_item.new_comp_currency;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom;
            L_ordloc_exp_log_tbl(L_log_tbl_cnt).reason_code       := 'ORDS';
            L_log_tbl_cnt := L_log_tbl_cnt +1;
            --
            --
         else  -- The order is not yet shipped, The assessments need to be checked.
            --

            -- Check if there are expense related assessments.
            open  C_ord_assessments(ord_item.order_no, ord_item.item, ord_item.comp_id);
            fetch C_ord_assessments into L_ord_assessments;
            if C_ord_assessments%FOUND then
               close C_ord_assessments;

               -- Verify that at least one of the custom entries is in worksheet status.
               open  C_custom_entry(ord_item.order_no, ord_item.item);
               fetch C_custom_entry into  L_custom_entry;
               if C_custom_entry%FOUND then
                  close C_custom_entry;
                  --
                  if L_custom_entry.status = 'W' then
                     -- Update the expense and the assessments that would get affected.
                     if ord_item.curr_comp_rate     != ord_item.old_comp_rate or
                        ord_item.curr_comp_currency != ord_item.old_comp_currency or
                        NVL(ord_item.curr_per_count, -999)     != NVL(ord_item.old_per_count, -999) or
                        NVL(ord_item.curr_per_count_uom, -999) != NVL(ord_item.old_per_count_uom, -999) then
                        --
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).item              := ord_item.item;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).pack_item         := ord_item.pack_item;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).location          := ord_item.location;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_currency := ord_item.curr_comp_currency;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_currency := ord_item.new_comp_currency;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).reason_code       := 'ORDU';
                        L_log_tbl_cnt := L_log_tbl_cnt +1;
                        --
                     end if;
                     --
                     -- Update the perticular Ordloc expense.
                     update ordloc_exp oe
                        set oe.comp_rate         = ord_item.new_comp_rate,
                            oe.comp_currency     = ord_item.new_comp_currency,
                            oe.per_count         = ord_item.new_per_count,
                            oe.per_count_uom     = ord_item.new_per_count_uom,
                            oe.exchange_rate     = decode(oe.comp_currency, ord_item.new_comp_currency, oe.exchange_rate, L_exchange_rate)
                      where oe.rowid = ord_item.row_id;
                     --
                     if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                               'PE',
                                               ord_item.item, -- item
                                               ord_item.supplier,
                                               NULL, -- item_exp_type
                                               NULL, -- item_exp_seq
                                               ord_item.order_no,
                                               NULL, -- ord_seq_no
                                               ord_item.pack_item, -- pack_item
                                               NULL, -- zone_id
                                               ord_item.location,
                                               NULL, -- hts
                                               NULL, -- import_country_id
                                               ord_item.origin_country_id,
                                               NULL, -- effect_from
                                               NULL) = FALSE then -- effect_to
                        return FALSE;
                     end if;

                     -- Make an entry into the rev plsql table, if it's not present.
                     L_ord_in_rev_tbl := 'N';
                     for cnt in 1..L_rev_ord_tbl_cnt-1 loop
                        if L_rev_orders_tbl(cnt).order_no = ord_item.order_no then
                           L_ord_in_rev_tbl := 'Y';
                           EXIT;
                        end if;
                     end loop;
                     --
                     if L_ord_in_rev_tbl = 'N' then
                        L_rev_orders_tbl(L_rev_ord_tbl_cnt).order_no := ord_item.order_no;
                        L_rev_ord_tbl_cnt := L_rev_ord_tbl_cnt + 1;
                     end if;            --

                     -- Getting the L_ship_to_date
                    open C_get_ship_to_date(ord_item.order_no);
                    fetch C_get_ship_to_date into L_ship_to_date;
                    close C_get_ship_to_date;

                     -- Loop through all assessments that would need recalculation due to expense update.
                     for assmnt in C_ord_assessments(ord_item.order_no, ord_item.item, ord_item.comp_id) loop

                        -- Recalculate the assessment
                        if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                                  'PA',
                                                  ord_item.item, -- item
                                                  NULL,
                                                  NULL, -- item_exp_type
                                                  NULL, -- item_exp_seq
                                                  ord_item.order_no,
                                                  NULL, -- ord_seq_no
                                                  ord_item.pack_item, -- pack_item
                                                  NULL, -- zone_id
                                                  NULL,
                                                  assmnt.hts, -- hts
                                                  assmnt.import_country_id, -- import_country_id
                                                  ord_item.origin_country_id,
                                                  assmnt.effect_from, -- effect_from
                                                  assmnt.effect_to) = FALSE then -- effect_to
                           return FALSE;
                        end if;
                        --
                        -- log the assessment update into local table variable
                        L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).order_no            := ord_item.order_no;
                        L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).item                := ord_item.item;
                        L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).import_country_id   := assmnt.import_country_id;
                        L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).hts                 := assmnt.hts;
                        L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).effect_from         := assmnt.effect_from;
                        L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).effect_to           := assmnt.effect_to;
                        L_mod_order_tbl_cnt := L_mod_order_tbl_cnt+1;
                        --
                        --

                        -- / Delete and redefault the custom entry charges
                        -- loop through all the custom entries and for all those having status as 'W'orksheet, delete and redefault the custom entry.
                        for cust_ent in C_custom_entry(ord_item.order_no, ord_item.item) loop
                           if cust_ent.status = 'W' then
                              -- Delete the custom entry, and call CE_CHARGES_SQL.INSERT_COMPS to redefault the custom entry.
                              delete from ce_charges
                                    where ce_id       = cust_ent.ce_id
                                      and order_no    = ord_item.order_no
                                      and item        = ord_item.item
                                      and hts         = assmnt.hts
                                      and effect_from = assmnt.effect_from
                                      and effect_to   = assmnt.effect_to
                                      and (ord_item.pack_item IS NULL
                                        or (ord_item.pack_item IS NOT NULL
                                            AND pack_item = ord_item.pack_item));
                              --
                              -- redefault the custom entry.
                              if CE_CHARGES_SQL.INSERT_COMPS(L_error_message,
                                                             cust_ent.ce_id,
                                                             cust_ent.vessel_id,
                                                             cust_ent.voyage_flt_id,
                                                             TO_DATE(cust_ent.estimated_depart_date, 'DD-MON-RR'),
                                                             ord_item.order_no,
                                                             ord_item.item,
                                                             ord_item.pack_item,
                                                             assmnt.hts,
                                                             assmnt.import_country_id,
                                                             TO_DATE(assmnt.effect_from, 'DD-MON-RR'),
                                                             TO_DATE(assmnt.effect_to,   'DD-MON-RR')) = FALSE then
                                 return FALSE;
                              end if;
                              --
                           end if;
                        end loop; -- for each custom entry.
                     end loop; -- for each assessment
                  else -- none of the custom entries are in Worksheet status
                     --
                     if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(L_error_message,
                                                         L_standard_uom,
                                                         L_standard_class,
                                                         L_conv_factor,
                                                         ord_item.item,
                                                         'N') = FALSE then
                        return FALSE;
                     end if;
                     --
                     -- Calculate the total of all the manifest quantities of the custom entries.
                     L_total_qty := 0;
                     for cust_ent in C_custom_entry(ord_item.order_no, ord_item.item) loop
                        --
                        if L_standard_uom != cust_ent.manifest_item_qty_uom then
                           if UOM_SQL.CONVERT(L_error_message,
                                              L_std_manifest_item_qty,
                                              L_standard_uom,
                                              cust_ent.manifest_item_qty,
                                              cust_ent.manifest_item_qty_uom,
                                              ord_item.item,
                                              ord_item.supplier,
                                              ord_item.origin_country_id) = FALSE then
                              return FALSE;
                           end if;
                        else
                           L_std_manifest_item_qty := cust_ent.manifest_item_qty;
                        end if;
                        --
                        L_total_qty := L_total_qty + L_std_manifest_item_qty;
                        --
                     end loop;
                     --
                     -- Get the order items total quenatiy.
                     open C_ord_qty(ord_item.order_no, ord_item.item);
                     fetch C_ord_qty into L_ordered_qty;
                     close C_ord_qty;
                     --
                     -- Check if the total manifest quantity is lesser than the ordered quantity.
                     if L_total_qty < L_ordered_qty then
                        -- Update the expense and the assessments that would get affected.
                        if UPDATE_EXP_RECAL_ASSMT (O_error_message,
                                                   L_ordloc_exp_log_tbl,
                                                   L_log_tbl_cnt,
                                                   L_mod_order_item_hts_tbl,
                                                   L_mod_order_tbl_cnt,
                                                   L_rev_orders_tbl,
                                                   L_rev_ord_tbl_cnt,
                                                   ord_item.row_id,
                                                   ord_item.item,
                                                   ord_item.supplier,
                                                   ord_item.order_no,
                                                   ord_item.order_status,
                                                   ord_item.pack_item,
                                                   ord_item.location,
                                                   ord_item.comp_id,
                                                   ord_item.origin_country_id,
                                                   ord_item.curr_comp_rate,
                                                   ord_item.curr_comp_currency,
                                                   ord_item.curr_per_count,
                                                   ord_item.curr_per_count_uom,
                                                   ord_item.old_comp_rate,
                                                   ord_item.old_comp_currency,
                                                   ord_item.old_per_count,
                                                   ord_item.old_per_count_uom,
                                                   L_exchange_rate,
                                                   ord_item.new_comp_rate,
                                                   ord_item.new_comp_currency,
                                                   ord_item.new_per_count,
                                                   ord_item.new_per_count_uom) = FALSE then
                           return FALSE;
                        end if;
                     else
                        -- the ordered quantity is not less than the manifest.
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).item              := ord_item.item;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).pack_item         := ord_item.pack_item;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).location          := ord_item.location;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_currency := ord_item.curr_comp_currency;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_currency := ord_item.new_comp_currency;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom;
                        L_ordloc_exp_log_tbl(L_log_tbl_cnt).reason_code       := 'CEVF';
                        L_log_tbl_cnt := L_log_tbl_cnt +1;
                        --
                     end if;  -- end total manifest qty < ordered qty
                     --
                  end if; -- custom entry in worksheet status.
               else
                  close C_custom_entry;
                  -- no custom entries present, update the expense and recalculate all the related assessments.

                  -- Update the expense and the assessments that would get affected.
                  if UPDATE_EXP_RECAL_ASSMT (O_error_message,
                                             L_ordloc_exp_log_tbl,
                                             L_log_tbl_cnt,
                                             L_mod_order_item_hts_tbl,
                                             L_mod_order_tbl_cnt,
                                             L_rev_orders_tbl,
                                             L_rev_ord_tbl_cnt,
                                             ord_item.row_id,
                                             ord_item.item,
                                             ord_item.supplier,
                                             ord_item.order_no,
                                             ord_item.order_status,
                                             ord_item.pack_item,
                                             ord_item.location,
                                             ord_item.comp_id,
                                             ord_item.origin_country_id,
                                             ord_item.curr_comp_rate,
                                             ord_item.curr_comp_currency,
                                             ord_item.curr_per_count,
                                             ord_item.curr_per_count_uom,
                                             ord_item.old_comp_rate,
                                             ord_item.old_comp_currency,
                                             ord_item.old_per_count,
                                             ord_item.old_per_count_uom,
                                             L_exchange_rate,
                                             ord_item.new_comp_rate,
                                             ord_item.new_comp_currency,
                                             ord_item.new_per_count,
                                             ord_item.new_per_count_uom) = FALSE then
                     return FALSE;
                  end if;
               end if;
            else -- assessments are not affected by the expense update
               close C_ord_assessments;
               --
               if ord_item.curr_comp_rate     != ord_item.old_comp_rate or
                  ord_item.curr_comp_currency != ord_item.old_comp_currency or
                  NVL(ord_item.curr_per_count, -999)     != NVL(ord_item.old_per_count, -999) or
                  NVL(ord_item.curr_per_count_uom, -999) != NVL(ord_item.old_per_count_uom, -999) then

                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).item              := ord_item.item;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).pack_item         := ord_item.pack_item;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).location          := ord_item.location;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_comp_currency := ord_item.curr_comp_currency;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_comp_currency := ord_item.new_comp_currency;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom;
                  L_ordloc_exp_log_tbl(L_log_tbl_cnt).reason_code       := 'ORDU';
                  L_log_tbl_cnt := L_log_tbl_cnt +1;

               end if;
               --
               -- Update the perticular Ordloc expense.
               update ordloc_exp oe
                  set oe.comp_rate         = ord_item.new_comp_rate,
                      oe.comp_currency     = ord_item.new_comp_currency,
                      oe.per_count         = ord_item.new_per_count,
                      oe.per_count_uom     = ord_item.new_per_count_uom,
                      oe.exchange_rate     = decode(oe.comp_currency, ord_item.new_comp_currency, oe.exchange_rate, L_exchange_rate)
                where oe.rowid = ord_item.row_id;
               --
               if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                         'PE',
                                         ord_item.item, -- item
                                         ord_item.supplier,
                                         NULL, -- item_exp_type
                                         NULL, -- item_exp_seq
                                         ord_item.order_no,
                                         NULL, -- ord_seq_no
                                         ord_item.pack_item, -- pack_item
                                         NULL, -- zone_id
                                         ord_item.location,
                                         NULL, -- hts
                                         NULL, -- import_country_id
                                         ord_item.origin_country_id,
                                         NULL, -- effect_from
                                         NULL) = FALSE then -- effect_to
                  return FALSE;
               end if;

               -- Make an entry into the rev table, if it is not present in db table of the plsql table.
               L_ord_in_rev_tbl := 'N';
               for cnt in 1..L_rev_ord_tbl_cnt-1 loop
                  if L_rev_orders_tbl(cnt).order_no = ord_item.order_no then
                     L_ord_in_rev_tbl := 'Y';
                     EXIT;
                  end if;
               end loop;
               --
               if L_ord_in_rev_tbl = 'N' then
                  L_rev_orders_tbl(L_rev_ord_tbl_cnt).order_no := ord_item.order_no;
                  L_rev_ord_tbl_cnt := L_rev_ord_tbl_cnt + 1;
               end if;            --

            end if; -- end if expense related assessments found

            -- calling calc comp to recompute the consolidated/dependent component values.
            if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                      'PA',
                                      ord_item.item,
                                      NULL,
                                      NULL,
                                      NULL,
                                      ord_item.order_no,
                                      NULL,
                                      ord_item.pack_item,
                                      NULL,
                                      NULL,
                                      NULL,
                                      ord_item.origin_country_id,
                                      NULL,
                                      NULL) = FALSE then
               return FALSE;
            end if;

            -- whenever the updated expense component has the inDuty flag +/- all the hts are updated and hence lot the hts values.
            if ord_item.nom_flag_2 IN ('+', '-') then
               for l_hts in C_ord_hts(ord_item.order_no, ord_item.item) loop

                  L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).order_no            := ord_item.order_no;
                  L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).item                := ord_item.item;
                  L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).import_country_id   := l_hts.import_country_id;
                  L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).hts                 := l_hts.hts;
                  L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).effect_from         := l_hts.effect_from;
                  L_mod_order_item_hts_tbl(L_mod_order_tbl_cnt).effect_to           := l_hts.effect_to;
                  L_mod_order_tbl_cnt := L_mod_order_tbl_cnt+1;
                  --
               end loop;
            end if;

         end if; -- order shipped.
      end loop; -- end loop for each selected record, for an item order.

      -- Approve the last order if it was unapproved.
      if L_prev_ord is not NULL and L_prevord_status = 'A' then
         if OTB_SQL.ORD_APPROVE_CASCADE(L_prev_ord,
                                O_error_message) = FALSE then
            return FALSE;
         end if;
      end if;
   end if; -- end if import ind
   --
   -- call the flush expenses to copy the contents of the plsql table.
   if FLUSH_CHANGES_EXPENSES(O_error_message,
                             L_ordloc_exp_log_tbl,
                             L_rev_orders_tbl,
                             L_mod_order_item_hts_tbl) = FALSE then
      return FALSE;
   end if;
   --
   delete gtt_cost_comp_upd;
   return TRUE;
   --
EXCEPTION
   when OTHERS then
      -- delete the contents of the global temporary table.
      delete gtt_cost_comp_upd;
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'EXP_COST_COMP_UPD_SQL.UPDATE_ORDLOC_EXP',
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_ORDLOC_EXP;

---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ASSESS_RECAL_ASSESS
-- Purpose: The function is for internal use and is called by the function UPDATE_ORDSKU_HTS_ASSESS.
-- This function updates the ordloc exp component values. It also calls ELC_CALC_SQL.CALC_COMP to
-- recalculate the order expenses depending on the upd_assess_ind it updates the associated
-- assessments as well,
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ASSESS_RECAL_ASSESS(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_ordsku_assess_log_tbl  IN OUT ordsku_assess_TBL,
                                    O_log_tbl_cnt            IN OUT NUMBER,
                                    O_mod_order_item_hts_tbl IN OUT mod_order_item_hts_TBL,
                                    O_mod_order_tbl_cnt      IN OUT NUMBER,
                                    O_rev_orders_tbl         IN OUT rev_ord_TBL,
                                    O_rev_ord_tbl_cnt        IN OUT NUMBER,
                                    I_row_id                 IN ROWID,
                                    I_item                   IN ITEM_MASTER.ITEM%TYPE,
                                    I_order_no               IN ORDHEAD.ORDER_NO%TYPE,
                                    I_ord_stat               IN ORDHEAD.STATUS%TYPE,
                                    I_seq_no                 IN ORDSKU_HTS.SEQ_NO%TYPE,
                                    I_comp_id                IN ELC_COMP.COMP_ID%TYPE,
                                    I_pack_item              IN ORDLOC_EXP.PACK_ITEM%TYPE,
                                    I_hts                    IN ORDSKU_HTS.HTS%TYPE,
                                    I_origin_country_id      IN ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                    I_import_country_id      IN ORDSKU_HTS.IMPORT_COUNTRY_ID%TYPE,
                                    I_effect_from            IN ORDSKU_HTS.EFFECT_FROM%TYPE,
                                    I_effect_to              IN ORDSKU_HTS.EFFECT_TO%TYPE,
                                    I_curr_comp_rate         IN ORDLOC_EXP.COMP_RATE%TYPE,
                                    I_curr_per_count         IN ORDLOC_EXP.PER_COUNT%TYPE,
                                    I_curr_per_count_uom     IN ORDLOC_EXP.PER_COUNT_UOM%TYPE,
                                    I_old_comp_rate          IN ORDLOC_EXP.COMP_RATE%TYPE,
                                    I_old_per_count          IN ORDLOC_EXP.PER_COUNT%TYPE,
                                    I_old_per_count_uom      IN ORDLOC_EXP.PER_COUNT_UOM%TYPE,
                                    I_new_comp_rate          IN ORDLOC_EXP.COMP_RATE%TYPE,
                                    I_new_per_count          IN ORDLOC_EXP.PER_COUNT%TYPE,
                                    I_new_per_count_uom      IN ORDLOC_EXP.PER_COUNT_UOM%TYPE)
RETURN BOOLEAN
IS
   L_ord_in_rev_tbl   VARCHAR2(1);
BEGIN
   --
   -- / Updating the Order Expense and making a log entry.
   if I_curr_comp_rate     != I_old_comp_rate or
      I_curr_per_count     != I_old_per_count or
      NVL(I_curr_per_count_uom, -999) != NVL(I_old_per_count_uom, -999) then

      -- Since the orders' item has been shipped, we put the record into the
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).order_no          := I_order_no;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).comp_id           := I_comp_id;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).old_comp_rate     := I_curr_comp_rate;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).old_per_count     := I_curr_per_count;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).old_per_count_uom := I_curr_per_count_uom;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).new_comp_rate     := I_new_comp_rate;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).new_per_count     := I_new_per_count;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).new_per_count_uom := I_new_per_count_uom;
      O_ordsku_assess_log_tbl(O_log_tbl_cnt).reason_code       := 'ORDU';
      O_log_tbl_cnt  := O_log_tbl_cnt + 1;
      --
   end if;
   --
   -- Update the perticular Ordloc expense.
   update ordsku_hts_assess oha
      set oha.comp_rate         = I_new_comp_rate,
          oha.per_count         = I_new_per_count,
          oha.per_count_uom     = I_new_per_count_uom
    where oha.rowid = I_row_id;

   -- Recalculate the assessment which has been updated.
   if ELC_CALC_SQL.CALC_COMP(O_error_message,
                             'PA',
                             I_item, -- item
                             NULL,
                             NULL,   -- item_exp_type
                             NULL,   -- item_exp_seq
                             I_order_no,
                             NULL,   -- ord_seq_no
                             I_pack_item, -- pack_item
                             NULL,   -- zone_id
                             NULL,
                             I_hts,  -- hts
                             I_import_country_id,      -- import_country_id
                             I_origin_country_id,
                             I_effect_from,            -- effect_from
                             I_effect_to) = FALSE then -- effect_to
      return FALSE;
   end if;
   --
   O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).order_no            := I_order_no;
   O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).item                := I_item;
   O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).import_country_id   := I_import_country_id;
   O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).hts                 := I_hts;
   O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).effect_from         := I_effect_from;
   O_mod_order_item_hts_tbl(O_mod_order_tbl_cnt).effect_to           := I_effect_to;
   O_mod_order_tbl_cnt := O_mod_order_tbl_cnt+1;
   --
   -- storing the updated order into rev_order table.
   -- Make an entry into the rev table, if it is not present in db table of the plsql table.
   L_ord_in_rev_tbl := 'N';
   for cnt in 1..O_rev_ord_tbl_cnt -1 loop
      if O_rev_orders_tbl(cnt).order_no = I_order_no then
         L_ord_in_rev_tbl := 'Y';
         EXIT;
      end if;
   end loop;
   --
   if L_ord_in_rev_tbl = 'N' then
      O_rev_orders_tbl(O_rev_ord_tbl_cnt).order_no := I_order_no;
      O_rev_ord_tbl_cnt := O_rev_ord_tbl_cnt + 1;
   end if;
   --
   return TRUE;
   --
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'EXP_COST_COMP_UPD_SQL.UPDATE_ASSESS_RECAL_ASSESS',
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ASSESS_RECAL_ASSESS;


---------------------------------------------------------------------------------------------
-- Function Name: FLUSH_CHANGES_ASSESSMENTS
-- Purpose: The function is called by the function UPDATE_ORDSKU_HTS_ASSESS. This function
--          copies all the data from the plsql tables sent as parameters to the respective tables.
--          The orders which were updated manually are inserted into the table ordsku_hts_assess_exc_log
--          When an approved order is updated or modified the record is entered into the table rev_orders.
---------------------------------------------------------------------------------------------
FUNCTION FLUSH_CHANGES_ASSESSMENTS(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_ordsku_assess_log_tbl IN      ordsku_assess_TBL,
                                   I_ordsku_mod_order_tbl  IN      mod_order_item_hts_TBL,
                                   I_rev_orders_tbl        IN      rev_ord_TBL)
RETURN BOOLEAN
IS
BEGIN

   for rec in 1..I_ordsku_assess_log_tbl.COUNT loop
      insert into cost_comp_exc_log
        (exception_type,
         order_no,
         comp_id,
         old_comp_rate,
         old_per_count,
         old_per_count_uom,
         new_comp_rate,
         new_per_count,
         new_per_count_uom,
         create_id,
         create_datetime,
         reason_code)
        values
        ('OA',
         I_ordsku_assess_log_tbl(rec).order_no,
         I_ordsku_assess_log_tbl(rec).comp_id,
         I_ordsku_assess_log_tbl(rec).old_comp_rate,
         I_ordsku_assess_log_tbl(rec).old_per_count,
         I_ordsku_assess_log_tbl(rec).old_per_count_uom,
         I_ordsku_assess_log_tbl(rec).new_comp_rate,
         I_ordsku_assess_log_tbl(rec).new_per_count,
         I_ordsku_assess_log_tbl(rec).new_per_count_uom,
         user,
         sysdate,
         I_ordsku_assess_log_tbl(rec).reason_code);
   end loop;
   --
   for rec in 1..I_ordsku_mod_order_tbl.COUNT loop

      -- insert the record if the records do not exist
      merge into mod_order_item_hts moi
      using (select 'x' from dual)
         on (moi.order_no          = I_ordsku_mod_order_tbl(rec).order_no
         and moi.item              = I_ordsku_mod_order_tbl(rec).item
         and moi.import_country_id = I_ordsku_mod_order_tbl(rec).import_country_id
         and moi.hts               = I_ordsku_mod_order_tbl(rec).hts
         and moi.effect_from       = I_ordsku_mod_order_tbl(rec).effect_from
         and moi.effect_to         = I_ordsku_mod_order_tbl(rec).effect_to
         and moi.unapprove_ind     = 'N'
         and moi.pgm_name          = 'batch_ordcostcompupd'
         and to_char(moi.updated_datetime,'DD-MON-YY') = to_char(L_vdate,'DD-MON-YY'))
      when not matched then
       insert (order_no,
               item,
               import_country_id,
               hts,
               effect_from,
               effect_to,
               unapprove_ind,
               pgm_name,
               updated_datetime)
              values
              (I_ordsku_mod_order_tbl(rec).order_no,
               I_ordsku_mod_order_tbl(rec).item,
               I_ordsku_mod_order_tbl(rec).import_country_id,
               I_ordsku_mod_order_tbl(rec).hts,
               I_ordsku_mod_order_tbl(rec).effect_from,
               I_ordsku_mod_order_tbl(rec).effect_to,
               'N',
               'batch_ordcostcompupd',
               L_vdate);

   end loop;
   --
   --
   for rec in 1..I_rev_orders_tbl.COUNT loop
      merge into rev_orders ro
      using (select 'x' from dual)
      on    (ro.order_no = I_rev_orders_tbl(rec).order_no)
      when not matched then
        insert (order_no)
        values (I_rev_orders_tbl(rec).order_no);
   end loop;
   --
   return TRUE;
   --
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'EXP_COST_COMP_UPD_SQL.FLUSH_CHANGES_ASSESSMENTS',
                                            to_char(SQLCODE));
      return FALSE;
END FLUSH_CHANGES_ASSESSMENTS;
---------------------------------------------------------------------------------------------
/* Need to plug in the UPDATE_ORDSKU_HTS_ASSESS function */ --- GAP356 Modification
---------------------------------------------------------------------------------------------
-- Function Name: UPDATE_ORDSKU_HTS_ASSESS
-- Purpose: The function is called by the function PROCESS_COST_COMP_UPDATES. This function
--          selects rows from the cost_comp_upd_stg which have the defaulting levels as 'I'tem,
--          and 'E'lc. It updates the Order item hts assessments.
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDSKU_HTS_ASSESS(O_error_message IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_Thread_no     IN      RESTART_CONTROL.NUM_THREADS%TYPE,
                                  I_Threads       IN      RESTART_CONTROL.NUM_THREADS%TYPE)
RETURN BOOLEAN
IS

   L_ordsku_assess_log_tbl                ordsku_assess_TBL;      -- to hold the orders which are shipped.
   L_rev_orders_tbl                       rev_ord_TBL;            -- holds the orders that need to be inserted into the REV Table.
   L_mod_order_item_hts_tbl               mod_order_item_hts_TBL; -- holds the assessments which were recalculated.

   -- Cursor used to loop through the records of the GTT.
   cursor C_ord_items
   is
   select gtt.row_id,
          gtt.order_no,
          gtt.item,
          gtt.supplier,
          gtt.origin_country_id,
          gtt.import_country_id,
          gtt.pack_item,
          gtt.hts,
          gtt.seq_no,
          gtt.effect_from,
          gtt.effect_to,
          gtt.comp_id,
          gtt.curr_comp_rate,
          gtt.curr_comp_currency,
          gtt.curr_per_count,
          gtt.curr_per_count_uom,
          gtt.old_comp_rate,
          gtt.old_comp_currency,
          gtt.old_per_count,
          gtt.old_per_count_uom,
          gtt.new_comp_rate,
          gtt.new_comp_currency,
          gtt.new_per_count,
          gtt.new_per_count_uom,
          gtt.shipped_ind,
          gtt.order_status,
          gtt.nom_flag_2,
          gtt.cvb_code
     from gtt_cost_comp_upd gtt
     order by order_no, item;

   L_prev_ord        ORDHEAD.ORDER_NO%TYPE := NULL;
   L_prevord_status     VARCHAR2(1)        := NULL;
   L_log_tbl_cnt        NUMBER := 1;
   L_rev_ord_tbl_cnt    NUMBER := 1;
   L_mod_order_tbl_cnt  NUMBER := 1;

   -- Cursor to fetch the custom entries.
   cursor C_custom_entry(I_ord_no ORDHEAD.ORDER_NO%TYPE, I_item ITEM_MASTER.ITEM%TYPE)
   is
   select distinct
          ceh.ce_id,
          coi.vessel_id,
          coi.voyage_flt_id,
          coi.estimated_depart_date,
          NVL(coi.manifest_item_qty, 0) manifest_item_qty,
          NVL(coi.manifest_item_qty_uom, 0) manifest_item_qty_uom,
          ceh.status,
          oh.supplier
     from ce_ord_item coi,
          ce_head ceh,
          ordhead oh
    where oh.order_no  = I_ord_no
      and coi.order_no = oh.order_no
      and coi.item     = I_item
      and ceh.ce_id    = coi.ce_id
    order by ceh.status desc;

   L_custom_entry C_custom_entry%ROWTYPE;

   -- Cursor to get the total ordered quantity
   cursor C_ord_qty(I_order ORDLOC.ORDER_NO%TYPE, I_item ORDLOC.ITEM%TYPE)
   is
   select SUM(ol.qty_ordered)
     from ordloc ol
    where ol.order_no = I_order
      and ol.item     = I_item;
   --
   --
   L_error_message         RTK_ERRORS.RTK_KEY%TYPE;
   L_standard_uom          UOM_CLASS.UOM%TYPE;
   L_standard_class        UOM_CLASS.UOM_CLASS%TYPE;
   L_conv_factor           ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_total_qty             NUMBER;
   L_std_manifest_item_qty NUMBER;
   L_ordered_qty           NUMBER;
   L_ord_in_rev_tbl        VARCHAR2(1);
   --
BEGIN
   --
   -- Populate the global temporary table with the selected order item records.
   insert into gtt_cost_comp_upd
   (      row_id,
          order_no,
          item,
          supplier,
          origin_country_id,
          import_country_id,
          pack_item,
          hts,
          seq_no,
          effect_from,
          effect_to,
          comp_id,
          curr_comp_rate,
          curr_per_count,
          curr_per_count_uom,
          old_comp_rate,
          old_per_count,
          old_per_count_uom,
          new_comp_rate,
          new_per_count,
          new_per_count_uom,
          order_status,
          nom_flag_2,
          cvb_code,
          shipped_ind)
   with ordsku_hts1 as
   -- This select query gets the data defaulted from Elc comp form.
     (select oha.rowid row_id,
             oha.order_no,
             oht.item,
             oh.supplier,
             oht.origin_country_id,
             oht.import_country_id,
             oht.pack_item,
             oht.hts,
             oha.seq_no,
             oht.effect_from,
             oht.effect_to,
             oha.comp_id,
             oha.comp_rate       curr_comp_rate,
             oha.per_count       curr_per_count,
             oha.per_count_uom   curr_per_count_uom,
             ccu.old_comp_rate,
             ccu.old_per_count,
             ccu.old_per_count_uom,
             ccu.new_comp_rate,
             ccu.new_per_count,
             ccu.new_per_count_uom,
             oh.status,
             oha.nom_flag_2,
             oha.cvb_code
        from cost_comp_upd_stg  ccu,
             ordsku_hts_assess  oha,
             ordsku_hts         oht,
             ordhead            oh,
             ordsku             os,
             ordloc             ol,
             exp_cost_comp_temp ecct
       where ccu.defaulting_level   = 'E'
         and ccu.comp_type          = 'A'
         and ccu.order_default_ind  = 'Y'
         and oha.comp_id  = ccu.comp_id
         and oha.seq_no   = oht.seq_no
         and oha.order_no = oht.order_no
         and oht.item     = os.item
         and oht.order_no = os.order_no
         and oh.order_no  = oha.order_no
         and oh.import_country_id = ccu.import_country_id
         and oh.status    IN ('S', 'W', 'A')
         and oh.pickup_date  >= (select max(ccus.effective_date)
                                   from cost_comp_upd_stg ccus
                                  where ccus.comp_id  = ccu.comp_id)
         and oh.order_no = ol.order_no
         and os.order_no = ol.order_no
         and os.item     = ol.item
         and oha.order_no = ol.order_no
         and oht.order_no = ol.order_no
         and oht.item     = ol.item
         and ccu.comp_id  = ecct.comp_id
         and oha.comp_id  = ecct.comp_id
         and oha.comp_id  not in ( select eoca.comp_id 
                                     from exp_ord_comp_assess eoca
                                    where oha.comp_id = eoca.comp_id
                                      and oha.seq_no = eoca.seq_no 
                                      and oha.order_no = eoca.order_no)
         and NVL(ol.qty_received,0)=0
         and not exists ( select 'x'
                            from cost_comp_upd_stg ccu2
                           where ccu2.defaulting_level = 'I'
                             and ccu2.comp_type        = 'A'
                             and ccu2.order_default_ind= 'Y'
                             and oha.comp_id           = ccu2.comp_id
                             and oht.item              = ccu2.item
                             and oht.hts               = ccu2.hts
                             and oht.effect_from       = ccu2.effect_from
                             and oht.effect_to         = ccu2.effect_to
                             and oht.import_country_id = ccu2.import_country_id
                             and oht.origin_country_id = ccu2.origin_country_id
                             and oh.pickup_date  >= (select max(ccus.effective_date)
                                                       from cost_comp_upd_stg ccus
                                                      where ccus.comp_id  = ccu.comp_id))
         and mod(oha.order_no, I_Threads)+1 = I_Thread_no),
   --
   ordsku_hts1a as  -- queries all the shipped orders
      (select oh.*,
                'Y' -- shipped indicator
           from ordsku_hts1 oh
          where exists (select 'x'
                  from shipment sh,
                       shipsku  ss
                 where sh.shipment    = ss.shipment
                   and sh.order_no    = oh.order_no
                   and ss.item        = oh.item
                   and rownum =1)),

   ordsku_hts2 as
   -- This select query gets the data defaulted from Item assessment form.
     (select oha.rowid row_id,
             oha.order_no,
             oht.item,
             oh.supplier,
             oht.origin_country_id,
             oht.import_country_id,
             oht.pack_item,
             oht.hts,
             oha.seq_no,
             oht.effect_from,
             oht.effect_to,
             oha.comp_id,
             oha.comp_rate,
             oha.per_count,
             oha.per_count_uom,
             ccu.old_comp_rate,
             ccu.old_per_count,
             ccu.old_per_count_uom,
             ccu.new_comp_rate,
             ccu.new_per_count,
             ccu.new_per_count_uom,
             oh.status,
             oha.nom_flag_2,
             oha.cvb_code
        from cost_comp_upd_stg    ccu,
             ordsku_hts_assess    oha,
             ordsku_hts           oht,
             ordhead              oh,
             ordsku               os,
             ordloc               ol,
             exp_cost_comp_temp   ecct
       where ccu.defaulting_level  = 'I'
         and ccu.comp_type         = 'A'
         and ccu.order_default_ind = 'Y'
         and oha.comp_id  = ccu.comp_id
         and oha.order_no = oht.order_no
         and oha.seq_no   = oht.seq_no
         and oht.item     = ccu.item
         and oht.hts               = ccu.hts
         and oht.effect_from       = ccu.effect_from
         and oht.effect_to         = ccu.effect_to
         and oht.import_country_id = ccu.import_country_id
         and os.order_no           = oha.order_no
         and os.item               = ccu.item
         and oht.origin_country_id = ccu.origin_country_id
         and oh.order_no           = oht.order_no
         and oh.status in ('S', 'W', 'A')
         and oh.pickup_date  >= (select max(effective_date)
                                   from cost_comp_upd_stg ccus
                                  where ccus.comp_id  = ccu.comp_id)
         and oh.order_no = ol.order_no
         and os.order_no = ol.order_no
         and os.item     = ol.item
         and oha.order_no = ol.order_no
         and oht.order_no = ol.order_no
         and oht.item     = ol.item
         and ccu.comp_id  = ecct.comp_id
         and oha.comp_id  = ecct.comp_id
         and NVL(ol.qty_received,0)=0
         and oha.comp_id  not in ( select eoca.comp_id 
                                     from exp_ord_comp_assess eoca
                                    where oha.comp_id = eoca.comp_id
                                      and oha.seq_no = eoca.seq_no 
                                      and oha.order_no = eoca.order_no)
         and mod(oha.order_no, I_Threads)+1 = I_Thread_no),
   --
   ordsku_hts2a as -- queries all the shipped orders
      (select oh.*,
                'Y' -- shipped indicator.
         from ordsku_hts2 oh
        where exists (select 'x'
                from shipment sh,
                     shipsku  ss
               where sh.shipment    = ss.shipment
                 and sh.order_no    = oh.order_no
                 and ss.item        = oh.item
                 and rownum =1))
   -- query all the orders taking union of the above data sets.
   select ordsku_hts1a.*
     from ordsku_hts1a
   union all
   select oh.*,
          'N'     -- shipped indicator
     from ordsku_hts1 oh
    where oh.row_id not in (select oha.row_id
                              from ordsku_hts1a oha)
   union all
   select ordsku_hts2a.*
     from ordsku_hts2a
   union all
   select oh.*,
          'N'
     from ordsku_hts2 oh
    where oh.row_id not in (select oha.row_id
                              from ordsku_hts2a oha);


   -- In all the above queries we have selected the orders rowids along with the other required values into the Global temporary table.
   --
   L_log_tbl_cnt     := 1;
   L_prev_ord        := NULL;
   L_prevord_status  := NULL;
   --
   -- Start looping through each of the order items captured in the global temporary table.
   for ord_item in C_ord_items loop
      --
      -- Approve the previous order if this is the second different order.
      if L_prev_ord is not NULL then
         if L_prev_ord != ord_item.order_no then
            -- New order being considered, hence approve the previous order
            if L_prevord_status = 'A' then
               if OTB_SQL.ORD_APPROVE_CASCADE(L_prev_ord,
                                      O_error_message) = FALSE then
                  return FALSE;
               end if;
            end if;
            --
            -- Unapprove the new order.
            if ord_item.order_status = 'A' then
               if OTB_SQL.ORD_UNAPPROVE_CASCADE(ord_item.order_no,
                                        O_error_message) = FALSE then
                  return FALSE;
               end if;
            end if;
            L_prev_ord       := ord_item.order_no;
            L_prevord_status := ord_item.order_status;
         end if;
      else
         if ord_item.order_status = 'A' then
            if OTB_SQL.ORD_UNAPPROVE_CASCADE(ord_item.order_no,
                                     O_error_message) = FALSE then
               return FALSE;
            end if;
         end if;
         L_prev_ord        := ord_item.order_no;
         L_prevord_status  := ord_item.order_status;
      end if;
      --
      if ord_item.shipped_ind = 'Y' then
         -- Since the orders' item has been shipped, we put the record into the
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom;
         L_ordsku_assess_log_tbl(L_log_tbl_cnt).reason_code       := 'ORDS';
         L_log_tbl_cnt := L_log_tbl_cnt +1;
         --
      else  -- The order is not yet shipped, the assessments need to be checked.
         --
         -- Verify that at least one of the custom entries is in worksheet status.
         open  C_custom_entry(ord_item.order_no, ord_item.item);
         fetch C_custom_entry into  L_custom_entry;
         if C_custom_entry%FOUND then
            close C_custom_entry;
            --
            if L_custom_entry.status = 'W' then
               -- update this assessment and also the other assessments which get affected through CVB.
               if UPDATE_ASSESS_RECAL_ASSESS(L_error_message,
                                             L_ordsku_assess_log_tbl,
                                             L_log_tbl_cnt,
                                             L_mod_order_item_hts_tbl,
                                             L_mod_order_tbl_cnt,
                                             L_rev_orders_tbl,
                                             L_rev_ord_tbl_cnt,
                                             ord_item.row_id,
                                             ord_item.item,
                                             ord_item.order_no,
                                             ord_item.order_status,
                                             ord_item.seq_no,
                                             ord_item.comp_id,
                                             ord_item.pack_item,
                                             ord_item.hts,
                                             ord_item.origin_country_id,
                                             ord_item.import_country_id,
                                             ord_item.effect_from,
                                             ord_item.effect_to,
                                             ord_item.curr_comp_rate,
                                             ord_item.curr_per_count,
                                             ord_item.curr_per_count_uom,
                                             ord_item.old_comp_rate,
                                             ord_item.old_per_count,
                                             ord_item.old_per_count_uom,
                                             ord_item.new_comp_rate,
                                             ord_item.new_per_count,
                                             ord_item.new_per_count_uom) = FALSE then
                  return FALSE;
               end if;
               --
               for cust_ent in C_custom_entry(ord_item.order_no, ord_item.item) loop
                  if cust_ent.status = 'W' then
                     -- Delete the custom entry, and call CE_CHARGES_SQL.INSERT_COMPS to redefault the custom entry.
                     delete from ce_charges
                           where ce_id       = cust_ent.ce_id
                             and order_no    = ord_item.order_no
                             and item        = ord_item.item
                             and hts         = ord_item.hts
                             and effect_from = ord_item.effect_from
                             and effect_to   = ord_item.effect_to
                             and (ord_item.pack_item IS NULL
                               or (ord_item.pack_item IS NOT NULL
                                   AND pack_item = ord_item.pack_item));
                     --
                     -- redefault the custom entry.
                     if CE_CHARGES_SQL.INSERT_COMPS(L_error_message,
                                                    cust_ent.ce_id,
                                                    cust_ent.vessel_id,
                                                    cust_ent.voyage_flt_id,
                                                    TO_DATE(cust_ent.estimated_depart_date, 'DD-MON-RR'),
                                                    ord_item.order_no,
                                                    ord_item.item,
                                                    ord_item.pack_item,
                                                    ord_item.hts,
                                                    ord_item.import_country_id,
                                                    TO_DATE(ord_item.effect_from, 'DD-MON-RR'),
                                                    TO_DATE(ord_item.effect_to,   'DD-MON-RR')) = FALSE then
                        return FALSE;
                     end if;
                     --
                  end if;
               end loop;
               --
            else -- None of the custom entries in worksheet status.
               if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(L_error_message,
                                                   L_standard_uom,
                                                   L_standard_class,
                                                   L_conv_factor,
                                                   ord_item.item,
                                                   'N') = FALSE then
                  return FALSE;
               end if;
               --
               -- Calculate the total of all the manifest quantities of the custom entries.
               for cust_ent in C_custom_entry(ord_item.order_no, ord_item.item) loop

                  if L_standard_uom != cust_ent.manifest_item_qty_uom then
                     if UOM_SQL.CONVERT(L_error_message,
                                        L_std_manifest_item_qty,
                                        L_standard_uom,
                                        cust_ent.manifest_item_qty,
                                        cust_ent.manifest_item_qty_uom,
                                        ord_item.item,
                                        ord_item.supplier,
                                        ord_item.origin_country_id) = FALSE then
                        return FALSE;
                     end if;
                  else
                     L_std_manifest_item_qty := cust_ent.manifest_item_qty;
                  end if;
                  --
                  L_total_qty := L_total_qty + L_std_manifest_item_qty;
                  --
               end loop;
               --
               -- Get the order items total quenatiy.
               open C_ord_qty(ord_item.order_no, ord_item.item);
               fetch C_ord_qty into L_ordered_qty;
               close C_ord_qty;

               -- Check if the total manifest quantity is lesser than the ordered quantity.
               if L_total_qty < L_ordered_qty then

                  if UPDATE_ASSESS_RECAL_ASSESS(L_error_message,
                                                L_ordsku_assess_log_tbl,
                                                L_log_tbl_cnt,
                                                L_mod_order_item_hts_tbl,
                                                L_mod_order_tbl_cnt,
                                                L_rev_orders_tbl,
                                                L_rev_ord_tbl_cnt,
                                                ord_item.row_id,
                                                ord_item.item,
                                                ord_item.order_no,
                                                ord_item.order_status,
                                                ord_item.seq_no,
                                                ord_item.comp_id,
                                                ord_item.pack_item,
                                                ord_item.hts,
                                                ord_item.origin_country_id,
                                                ord_item.import_country_id,
                                                ord_item.effect_from,
                                                ord_item.effect_to,
                                                ord_item.curr_comp_rate,
                                                ord_item.curr_per_count,
                                                ord_item.curr_per_count_uom,
                                                ord_item.old_comp_rate,
                                                ord_item.old_per_count,
                                                ord_item.old_per_count_uom,
                                                ord_item.new_comp_rate,
                                                ord_item.new_per_count,
                                                ord_item.new_per_count_uom) = FALSE then
                     return FALSE;
                  end if;

               else -- the ordered quantity is not less than the manifest.
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).order_no          := ord_item.order_no;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).comp_id           := ord_item.comp_id;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).old_comp_rate     := ord_item.curr_comp_rate;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).old_per_count     := ord_item.curr_per_count;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).old_per_count_uom := ord_item.curr_per_count_uom;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).new_comp_rate     := ord_item.new_comp_rate;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).new_per_count     := ord_item.new_per_count;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).new_per_count_uom := ord_item.new_per_count_uom;
                  L_ordsku_assess_log_tbl(L_log_tbl_cnt).reason_code       := 'CEVF';
                  L_log_tbl_cnt := L_log_tbl_cnt +1;
               end if;  -- end total manifest qty < ordered qty
            end if;
         else -- no custom entries present.
            close C_custom_entry;
            -- update this assessment and also the other assessments which get affected through CVB.

            if UPDATE_ASSESS_RECAL_ASSESS(L_error_message,
                                          L_ordsku_assess_log_tbl,
                                          L_log_tbl_cnt,
                                          L_mod_order_item_hts_tbl,
                                          L_mod_order_tbl_cnt,
                                          L_rev_orders_tbl,
                                          L_rev_ord_tbl_cnt,
                                          ord_item.row_id,
                                          ord_item.item,
                                          ord_item.order_no,
                                          ord_item.order_status,
                                          ord_item.seq_no,
                                          ord_item.comp_id,
                                          ord_item.pack_item,
                                          ord_item.hts,
                                          ord_item.origin_country_id,
                                          ord_item.import_country_id,
                                          ord_item.effect_from,
                                          ord_item.effect_to,
                                          ord_item.curr_comp_rate,
                                          ord_item.curr_per_count,
                                          ord_item.curr_per_count_uom,
                                          ord_item.old_comp_rate,
                                          ord_item.old_per_count,
                                          ord_item.old_per_count_uom,
                                          ord_item.new_comp_rate,
                                          ord_item.new_per_count,
                                          ord_item.new_per_count_uom) = FALSE then
               return FALSE;
            end if;
         end if;
      end if; -- end if Order shipped.
   end loop; -- end loop for each selected record, for an item order.
   --
   if L_prev_ord is not NULL and L_prevord_status = 'A' then
      if OTB_SQL.ORD_APPROVE_CASCADE(L_prev_ord,
                             O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;
   --
   -- Call the flush assessments function to copy the contents of the plsql tables to the respective database tables.
   if FLUSH_CHANGES_ASSESSMENTS(O_error_message,
                                L_ordsku_assess_log_tbl,
                                L_mod_order_item_hts_tbl,
                                L_rev_orders_tbl) = FALSE then
      return FALSE;
   end if;
   --
   delete gtt_cost_comp_upd;  -- commented for testing.
   return TRUE;
   --
EXCEPTION
   when OTHERS then
      -- delete the contents of the global temporary table.
      delete gtt_cost_comp_upd;

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'EXP_COST_COMP_UPD_SQL.UPDATE_ORDSKU_HTS_ASSESS',
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_ORDSKU_HTS_ASSESS;

---------------------------------------------------------------------------------------------
-- Function Name: PROCESS_ORD_COST_COMP_UPDATES
-- Purpose: The function is called by a batch/shell script which is scheduled to run at the end of the day.
--          The function calls the functions to cascade the HTS, EXPENSES to the orders.
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_ORD_COST_COMP_UPDATES (O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_Thread_no              IN      RESTART_CONTROL.NUM_THREADS%TYPE,
                                        I_Threads                IN      RESTART_CONTROL.NUM_THREADS%TYPE)
RETURN BOOLEAN
IS
   cursor C_sys_opts
   is
   select *
     from system_options;

BEGIN

   open  C_sys_opts;
   fetch C_sys_opts into L_system_options;
   close C_sys_opts;

   L_vdate := get_vdate();

   -- Update Order expenses which is casacaded from ELC comp, Expense profile and Item expense forms.
   if  UPDATE_ORDLOC_EXP(O_error_message,
                         I_Thread_no,
                         I_Threads) = FALSE then
      return FALSE;
   end if;

   if L_system_options.import_ind = 'Y' then
      -- Update Order Assessments which is casacaded from ELC comp, and Item HTS assessments forms.
      if  UPDATE_ORDSKU_HTS_ASSESS(O_error_message,
                                   I_Thread_no,
                                   I_Threads) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'EXP_COST_COMP_UPD_SQL.PROCESS_ORD_COST_COMP_UPDATES',
                                            to_char(SQLCODE));
      return FALSE;

END PROCESS_ORD_COST_COMP_UPDATES;
------------------------------------------------------------------------------------------------------
END EXP_COST_COMP_UPD_SQL;
/