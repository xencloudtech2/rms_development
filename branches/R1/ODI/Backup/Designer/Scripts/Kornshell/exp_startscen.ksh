#!/bin/ksh
#######################################################################
##Modification History:                                         
##Date     Name                 Company  Modification Description        
##-------- -------------------- -------- ------------------------------
# 12/19/13 Vinh Mai             Express  Added Key Words, Agent, Log Level
#######################################################################

usage(){
	print exp_startscen.ksh - a wrapper to execute ODI Scenarios
	print Usage: exp_startscen.ksh -s scenario_name [-c context_code] [-v version]
	print   -s   ODI scenario name
	print   [-a]   Agent Name
    print   [-c]   Context Code (DEV|TEST|PROD)
    print   [-k]   Key Words	
    print   [-l]   Log Level (1|2|3|4|5|6)	
	print   [-v]   version (if it is empty, the latest version will be used)
}



if [ $# -lt 2 ]; then
   usage;
   exit -1;
fi

# Set EDITOR var to vi if not already set to something
EDITOR=${EDITOR:-vi}

if [ $ODI_HOME =  ]; then
        MY_DIR=$(dirname $0)
        sdir=$(pwd)
        cd ${MY_DIR}/..
        ODI_HOME=$(pwd)
        cd $sdir
        export ODI_HOME
fi

. $ODI_HOME/bin/odiparams.sh
# Default Values
scenario_name=
context_code=DEV
agent_name=
version_number=-1
filename=
odi_keywords=
scriptPID=$$



while getopts :s:c:v:l arg
do
	case $arg in
	    s)
	    	scenario_name=$OPTARG
			filename=$scenario_name.$(date +%Y%m%d%H%M%S).log
		;;
	    a)
	    	agent_name=$OPTARG
		;;
	    c)
	    	context_code=$OPTARG
		;;
	    k)
	    	odi_keywords=$OPTARG
		;;
	    l)
	    	log_level=$OPTARG
		;;
	    v)
	    	version_number=$OPTARG
		;;
	    h|*)
	    	usage
		exit 1
		;;
	esac
done

$ODI_JAVA_START0 -DLOG_FILE=$filename -classpath $ODI_CLASSPATH oracle.odi.StartScen $scenario_name $version_number $context_code $log_level $odi_keywords -NAME=$agent_name EXPR_ODI_PROJECT.EXP_UNIX_PID=${scriptPID}
exit $?
