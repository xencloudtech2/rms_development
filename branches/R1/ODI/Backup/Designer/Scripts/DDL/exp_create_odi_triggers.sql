/***********************************************************************
/*  Name: exp_create_odi_triggers
/*  Description: Sample DBI table for ODI framework
/* Modification History: 
/* Date      Name                Modification Description
/* --------  -----------------   -------------------------------------
/* 
/**********************************************************************/

 -- Add Triggers:
CREATE OR REPLACE TRIGGER exp_odi_app_trg1
      BEFORE INSERT OR UPDATE ON EXP_ODI_APPLICATION
      FOR EACH ROW
BEGIN
:NEW.LAST_UPDATE_DATETIME := SYSDATE;
:NEW.LAST_UPDATE_ID := USER;
END;
/
CREATE OR REPLACE TRIGGER exp_odi_patt_trg1
      BEFORE INSERT OR UPDATE ON EXP_ODI_PATTERN
      FOR EACH ROW
BEGIN
:NEW.LAST_UPDATE_DATETIME := SYSDATE;
:NEW.LAST_UPDATE_ID := USER;
END;
/
CREATE OR REPLACE TRIGGER exp_odi_intf_trg1
      BEFORE INSERT OR UPDATE ON EXP_ODI_INTERFACE
      FOR EACH ROW
BEGIN
:NEW.LAST_UPDATE_DATETIME := SYSDATE;
:NEW.LAST_UPDATE_ID := USER;
END;
/
CREATE OR REPLACE TRIGGER exp_odi_param_trg1
      BEFORE INSERT OR UPDATE ON EXP_ODI_PATTERN_PARAM
      FOR EACH ROW
BEGIN
:NEW.LAST_UPDATE_DATETIME := SYSDATE;
:NEW.LAST_UPDATE_ID := USER;
END;
/
CREATE OR REPLACE TRIGGER exp_odi_intf_param_trg1
      BEFORE INSERT OR UPDATE ON EXP_ODI_INTERFACE_PARAM
      FOR EACH ROW
BEGIN
:NEW.LAST_UPDATE_DATETIME := SYSDATE;
:NEW.LAST_UPDATE_ID := USER;
END;
/
