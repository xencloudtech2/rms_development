/***********************************************************************
/*  Name: exp_create_exp_interface_file_stg_table.sql
/*  Description: Create EXP_INTERFACE_FILE_STG table.
/* Modification History: 
/* Date      Name                Modification Description
/* --------  -----------------   -------------------------------------
/* 3/3/2014  Vinh Mai            Initial Version
/**********************************************************************/

-- Create table for the files to be staged for loading
DROP TABLE EXPR_INTEGRATION.EXP_INTERFACE_FILE_STG;
CREATE TABLE EXPR_INTEGRATION.EXP_INTERFACE_FILE_STG
  (
    INTERFACE_ID   VARCHAR2(50) NOT NULL,
    FILENAME       VARCHAR2(100) NOT NULL,
    FILE_STATUS     VARCHAR2(1) NOT NULL,
	SEQUENCE_NUM    NUMBER NOT NULL,
    LAST_UPDATE_ID VARCHAR2(80),
    LAST_UPDATE_DATETIME DATE,
  CONSTRAINT PK_INTERFACE_FILE_STG PRIMARY KEY (INTERFACE_ID, FILENAME,SEQUENCE_NUM),
    CONSTRAINT INTERFACE_ID_EOI_EIFS_FK FOREIGN KEY (INTERFACE_ID) REFERENCES EXPR_INTEGRATION.EXP_ODI_INTERFACE (INTERFACE_ID),
	CONSTRAINT CHK_EIFS_FILE_STATUS_IND CHECK (FILE_STATUS IN ('N','L','A')) ENABLE
  );
set define off
CREATE OR REPLACE TRIGGER EXPR_INTEGRATION.EXP_ODI_INTF_STG_TRG1 BEFORE
  INSERT OR
  UPDATE ON EXP_INTERFACE_FILE_STG FOR EACH ROW BEGIN :NEW.LAST_UPDATE_DATETIME := SYSDATE;
  :NEW.LAST_UPDATE_ID                                                            := USER;
END;
/
ALTER TRIGGER EXPR_INTEGRATION.EXP_ODI_INTF_STG_TRG1 ENABLE;
 