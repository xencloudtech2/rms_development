/***********************************************************************
/*  Name: exp_insert_ffi_pattern_param.sql
/*  Description: ODI FFI Pattern parameters
/* Modification History: 
/* Date      Name                Modification Description
/* --------  -----------------   -------------------------------------
/* 1/15/2014 Vinh Mai            Initial Version
/* 2/19/2014 Vinh Mai            Added new params: MULTI_FILENAME_MASK
/**********************************************************************/
-- Insert new pattern parameters
delete from EXP_ODI_PATTERN_PARAM where pattern_type = 'FFI';
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','ARCHIVE_DIR','Archive directory location','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','ARCHIVE_FILE','Archive Default Flat file name','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','RESTART_NAME_ARCHIVE','RMS Restart Program Name for Archive Process','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','RESTART_NAME_STAGING','RMS Restart Program Name for Staging Process','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','RESTART_NAME','RMS Restart Program Name.','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','SOURCE_DIR','The name of the inbound file.','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','SOURCE_FILE','The directory of the inbound file.','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','MULTI_FILENAME_MASK','The file mask used to retrieve a list of files from the source directory','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','STAGING_TABLE','Name of the staging table','Y','V');
