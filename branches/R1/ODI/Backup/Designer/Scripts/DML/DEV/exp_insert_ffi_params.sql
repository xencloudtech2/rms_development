/***********************************************************************
/*  Name: exp_insert_ffi_params.sql
/*  Description: ODI FFI Pattern parameters
/* Modification History: 
/* Date      Name                Modification Description
/* --------  -----------------   -------------------------------------
/* 
/**********************************************************************/
-- Insert new pattern parameters
delete from EXP_ODI_PATTERN_PARAM where pattern_type = 'FFI';
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','ARCHIVE_DIR','Archive directory location','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','ARCHIVE_FILE','Archive Default Flat file name','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','RESTART_NAME_ARCHIVE','RMS Restart Program Name for Archive Process','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','RESTART_NAME_STAGING','RMS Restart Program Name for Staging Process','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','RESTART_NAME','RMS Restart Program Name.','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','SOURCE_DIR','The name of the inbound file.','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('FFI','SOURCE_FILE','The directory of the inbound file.','Y','V');

commit;
